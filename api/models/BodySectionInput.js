/**
 * bodySectionInput.js
 *
 * the sections of a form body
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    inputType:{
      type:'string',
      isIn: [
        //basic
        'text',
        'textarea',
        'e-mail',
        'number',
        'money',
        'address',
        'date',
        'hour',
        'date-hour',
        'cellphone',
        //selection
        'only-one',
        'multiple',
        'yes-no',
        'gender',
        'ranking',
        //other
        'geolocation',
        'photo',
      ],
    },

    label: {
      type: 'string',
      required:true
    },

    order:{
      type:'number'
    },

    help: {
      type: 'string'
    },

    isRequired:{
      type:'boolean'
    },

    includeInFilters:{
      type:'boolean'
    },

    maxLength:{
      type:'number',
      allowNull: true
    },

    minLength:{
      type:'number',
      allowNull: true
    },

    maxValue:{
      type:'number',
      allowNull: true
    },

    minValue:{
      type:'number',
      allowNull: true
    },

    is12h:{
      type:'boolean'
    },

    selectType:{
      type:'string',
      isIn: [
        'select',//<select>
        'radio',//radio or check-box
      ],
    },

    domain:{
      model:'domain',
    },

    canTypeOther:{
      type:'boolean'
    },

    labelCanTypeOther: {
      type: 'string',
      maxLength: 255
    },

    yesText: {
      type: 'string',
      maxLength: 255
    },

    noText: {
      type: 'string',
      maxLength: 255
    },

    minStep:{
      type:'number',
      defaultsTo:0
    },

    maxStep:{
      type:'number',
      defaultsTo:10
    },

    stepValue:{
      type:'number',
      defaultsTo:1
    },

    editableLocation:{
      type:'boolean'
    },

    photoSource:{
      type:'string',
      isIn: [
        'camera',
        'gallery',
        'both'
      ],
    },

    imageQuality:{
      type:'number'
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    // n/a

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    // n/a

    bodySection:{
      model:'bodySection',
      description:'the bodySection to which this input belongs.'
    },

    values: {
      collection:'selectOption',
      via: 'input'
    },

  },

};
