/**
 * Form.js
 *
 * The forms
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    name: {
      type: 'string',
      required: true,
      maxLength: 255
    },

    description: {
      type: 'string'
    },

    isPublic:{
      type:'boolean',
      description:'is public or not',
      defaultsTo:true
    },

    isActive:{
      type:'boolean',
      description:'is active or not',
      defaultsTo:false
    },

    isPublished:{
      type:'boolean',
      description:'is published or not, set once, leave forever',
      defaultsTo:false
    },

    header:{
      type:'string',
      required:false
    },

    footer:{
      type:'string',
      required:false
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    // n/a

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    // n/a

    bodySections: {
      collection:'bodySection',
      via: 'form'
    },

    routeGroups:{
      collection:'routeGroup',
      via:'form'
    },

    users: {
      collection:'user',
      via: 'forms'
    },

  },

};
