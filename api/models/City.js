/**
 * City.js
 *
 * A city on this planet
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    name: {
      type: 'string',
      required: true,
      maxLength: 255
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    // n/a

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    // n/a

    state:{
      model:'state',
      description:'the state or department this city is located on.'
    },

    neighborhoods: {
      collection:'neighborhood',
      via: 'city'
    },

  },

  //custom methods

  createNested: async function(params) {
    var newRecord = await City.create({
      name: params.city.name,
      state:params.state,
    })
    .fetch();

    newRecord.neighborhoods=[];

    for(var ne=0;ne<params.city.newNeighborhoods.length;ne++){
      newRecord.neighborhoods.push(await Neighborhood.create({
        name: params.city.newNeighborhoods[ne].name,
        city: newRecord.id,
      }));
    }

    return newRecord;
  },

};
