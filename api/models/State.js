/**
 * State.js
 *
 * A state or department on this planet
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    name: {
      type: 'string',
      required: true,
      maxLength: 255
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝
    // n/a

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    // n/a

    country:{
      model:'country',
      description:'the country this city is located on.'
    },

    cities: {
      collection:'city',
      via: 'state'
    },

  },

  //custom methods

  createNested: async function(params) {
    var newRecord = await State.create({
      name: params.state.name,
      country:params.country,
    })
    .fetch();

    newRecord.cities=[];

    for(var ci=0;ci<params.state.newCities.length;ci++){
      newRecord.cities.push(await City.createNested({
        city: params.state.newCities[ci],
        state:newRecord.id,
      }));
    }

    return newRecord;
  },

};
