module.exports = {

  friendlyName: 'Reporter',

  inputs: {
    form:{
      type:'string',
      required:true
    }
  },


  exits: {
  },

  fn: async function(inputs,exits){
    var records=await Record.find({
      form:inputs.form
    }).populate('answers');
    for(var re=0;re<records.length;re++){
      records[re].geo=await GeoAudit.findOne({
        id:records[re].geo,
      });
      records[re].user=await User.findOne({
        id:records[re].user,
      });
      records[re].place=await RoutePlace.findOne({
        id:records[re].place,
      }).populate('neighborhood');
      if(records[re].place){
        records[re].place.neighborhood.city=await City.findOne({
          id:records[re].place.neighborhood.city,
        }).populate('state');
        records[re].place.neighborhood.city.state.country=await Country.findOne({
          id:records[re].place.neighborhood.city.state.country,
        });
      }else{
        records[re].place={
          address:'',
          name:'',
          neighborhood:{
            name:'',
            city:{
              name:'',
              state:{
                name:'',
                country:{
                  name:''
                }
              }
            }
          }
        }
      }
    }

    return this.res.json(records);
  }

};
