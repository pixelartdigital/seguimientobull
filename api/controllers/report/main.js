module.exports = {

  friendlyName: 'Reporter',

  inputs: {
  },


  exits: {
  },

  fn: async function(inputs,exits){
    var forms=await Form.find({
      isPublic:true,
    }).populate('bodySections');
    for (var f = forms.length - 1; f >= 0; f--) {
      for (var i = forms[f].bodySections.length - 1; i >= 0; i--) {
        forms[f].bodySections[i].inputs=await BodySectionInput.find({
          bodySection:forms[f].bodySections[i].id
        }).populate('values');
        for (var inp = forms[f].bodySections[i].inputs.length - 1; inp >= 0; inp--) {
          switch(forms[f].bodySections[i].inputs[inp].inputType){
            case 'text':
            case 'textarea':
            case 'e-mail':
            case 'only-one':
            case 'multiple':
            case 'yes-no':
            case 'gender':
            case 'date':
            case 'hour':
            case 'date-hour':
            case 'cellphone':
            case 'geolocation':
              forms[f].bodySections[i].inputs[inp].alphaNumeric=true;
              break;
            case 'number':
            case 'money':
            case 'ranking':
              forms[f].bodySections[i].inputs[inp].numeric=true;
              break;
            case 'photo':
              forms[f].bodySections[i].inputs[inp].photo=true;
              break;
          }
        }
      }
    }

    return this.res.view('pages/report/main',{
      forms:forms
    });
  }

};
