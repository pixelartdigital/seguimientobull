module.exports = {

  friendlyName: 'show gallery search',

  inputs: {

  },


  exits: {

  },

  fn: async function(inputs,exits){
    var forms=await Form.find();
    return this.res.view('pages/gallery/show',{
      forms:forms
    });
  }

};
