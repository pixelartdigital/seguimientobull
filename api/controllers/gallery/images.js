module.exports = {

  friendlyName: 'The images loaded',

  inputs: {
    form:{
      type:'string'
    },
  },


  exits: {
  },

  fn: async function(inputs,exits){

    var forms=(!inputs.form)?[]:await Form.find({
      id:inputs.form
    }).populate('bodySections');
    var inputs=[];
    for(var fo=0;fo<forms.length;fo++){
      var bodySections=forms[fo].bodySections;
      for(var bs=0;bs<bodySections.length;bs++){
        bodySections[bs].inputs=await BodySectionInput.find({
          bodySection:bodySections[bs].id
        }).populate('values');
        for(var inp=0;inp<bodySections[bs].inputs.length;inp++){
          if(bodySections[bs].inputs[inp].inputType=='photo'){
            inputs.push(bodySections[bs].inputs[inp].id);
          }
        }
      }
    }

    var answers=(inputs.length==0)?[]:await Answer.find({
      input:inputs
    }).populate("input");
    
    return this.res.json(answers);
  }

};
