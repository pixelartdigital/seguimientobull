module.exports = {


  friendlyName: 'Update password',


  description: 'Update the password for a user.',


  inputs: {

    password: {
      description: 'The new, unencrypted password.',
      example: 'abc123v2',
      required: true,
    },

    user:{
      type:'string',
      required:true,
    },

  },


  fn: async function (inputs, exits) {

    // Hash the new password.
    var hashed = await sails.helpers.passwords.hashPassword(inputs.password);

    // Update the record for a user.
    await User.update({ id: inputs.user })
    .set({
      password: hashed
    });

    return exits.success();

  }


};
