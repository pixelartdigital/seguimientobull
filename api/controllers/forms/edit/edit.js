module.exports = {

  friendlyName: 'Create new form',

  inputs: {

    id:  {
      required: true,
      type: 'string'
    },

    description: {
      type: 'string'
    },

    name:  {
      required: true,
      type: 'string'
    },

    isActive:{
      type:'boolean'
    },

    isPublic:{
      type:'boolean'
    }

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var editFormRecord = await Form.update({
      id:inputs.id
    },{
      name: inputs.name,
      description: inputs.description,
      isActive: inputs.isActive,
      isPublic: inputs.isPublic,
    })
    .fetch();

    // Since everything went ok, send our 200 response.
    return exits.success(editFormRecord);

  }

};
