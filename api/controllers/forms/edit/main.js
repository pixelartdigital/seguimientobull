module.exports = {

  friendlyName: 'Create new form',

  inputs: {
    id:{
      type:'string',
      required:true
    },
    section:{
      type:'string'
    }
  },


  exits: {

  },

  fn: async function(inputs,exits){
    var form=await Form.findOne({
      id:inputs.id
    });
    if(!inputs.section){
      inputs.section='main';
    }
    return this.res.view('pages/forms/edit/'+inputs.section,{
      form:form
    });
  }

};
