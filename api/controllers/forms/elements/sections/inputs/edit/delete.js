module.exports = {

  friendlyName: 'delete form section input',

  inputs: {

    deletedInput: {
      type: 'number',
    },

    modifiedInputs:  {
      type: [{}],
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {
    await BodySectionInput.destroy({
      id:inputs.deletedInput
    });
    for(var ip=0;ip<inputs.modifiedInputs.length;ip++){
      await BodySectionInput.update({
        id:inputs.modifiedInputs[ip].id
      }).set({
        order:inputs.modifiedInputs[ip].order
      });
    }

    // Since everything went ok, send our 200 response.
    return exits.success();

  }

};
