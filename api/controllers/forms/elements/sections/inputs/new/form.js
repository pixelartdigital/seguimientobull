module.exports = {

  friendlyName: 'New section input',

  inputs: {
    form:{
      type:'string',
      required:true
    },
    bodySection:{
      type:'string',
      required:true
    },
  },

  exits: {

  },

  fn: async function(inputs,exits){
    /*var nextOrder=1;
    var maxinput= await BodySectionInput.find({
      select:['order'],
      limit: 1,
      sort: 'order DESC',
      where:{
        bodySection:inputs.bodySection
      }
    });
    if(maxinput.length>0){
      nextOrder=maxinput[0].order+1;
    }*/
    return this.res.view('pages/forms/elements/sections/inputs/new',{
      form:inputs.form,
      bodySection:inputs.bodySection
    });
  }

};
