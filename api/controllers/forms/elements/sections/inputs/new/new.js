module.exports = {

  friendlyName: 'Create new form section input',

  inputs: {

    form: {
      type: 'number',
      required:true,
    },

    bodySection:  {
      type: 'number',
      required:true,
    },

    inputType:{
      type:'string',
      required:true
    },

    label:{
      type:'string',
      required:true
    },

    help:{
      type:'string',
    },

    isRequired:{
      type:'boolean',
    },

    includeInFilters:{
      type:'boolean',
    },

    maxLength:{
      type:'number',
    },

    minLength:{
      type:'number',
    },

    maxValue:{
      type:'number',
    },

    minValue:{
      type:'number',
    },

    is12h:{
      type:'boolean',
    },

    selectType:{
      type:'string',
    },

    domain:{
      type:'number',
    },

    canTypeOther:{
      type:'boolean',
    },

    labelCanTypeOther:{
      type:'string',
    },

    yesText:{
      type:'string',
    },

    noText:{
      type:'string',
    },

    minStep:{
      type:'number',
    },

    maxStep:{
      type:'number',
    },

    stepValue:{
      type:'number',
    },

    editableLocation:{
      type:'boolean',
    },

    photoSource:{
      type:'string',
    },

    imageQuality:{
      type:'number',
    },

    newDomainName:{
      type:'string',
    },

    newDomainDescription:{
      type:'string',
    },

    newDomainOptions:{
      type:[{}],
    },

    selectOptions:{
      type:[{}],
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    // Build up data for the new user record and save it to the database.
    // (Also use `fetch` to retrieve the new ID so that we can use it below.)
    var nextOrder=1;
    var maxbodySectionInput= await BodySectionInput.find({
      select:['order'],
      limit: 1,
      sort: 'order DESC',
      where:{
        bodySection:inputs.bodySection
      }
    });
    if(maxbodySectionInput.length>0){
      nextOrder=maxbodySectionInput[0].order+1;
    }
    if((inputs.inputType=="only-one"||inputs.inputType=="multiple")&&(inputs.optionOrigin=='domain'&&inputs.domain==-1)){
      var newDomain=await Domain.create({
        name:inputs.newDomainName,
        description:inputs.newDomainDescription
      });
      inputs.domain=newDomain.id;
      for(var i=inputs.newDomainOptions.length;i>=0;i++){
        inputs.newDomainOptions[i].domain=inputs.domain;
      }
      await DomainValue.createEach(inputs.newDomainOptions);
    }
    if(inputs.domain==-1){
      inputs.domain=null;
    }
    var newInputRecord = await BodySectionInput.create({
      inputType: inputs.inputType,
      label: inputs.label,
      bodySection: inputs.bodySection,
      order: nextOrder,
      help: inputs.help,
      isRequired: inputs.isRequired,
      includeInFilters: inputs.includeInFilters,
      maxLength: inputs.maxLength,
      minLength: inputs.minLength,
      maxValue: inputs.maxValue,
      minValue: inputs.minValue,
      is12h: inputs.is12h,
      selectType: inputs.selectType,
      domain: inputs.domain,
      canTypeOther: inputs.canTypeOther,
      labelCanTypeOther: inputs.labelCanTypeOther,
      yesText: inputs.yesText,
      noText: inputs.noText,
      minStep: inputs.minStep,
      maxStep: inputs.maxStep,
      stepValue: inputs.stepValue,
      editableLocation: inputs.editableLocation,
      photoSource: inputs.photoSource,
      imageQuality: inputs.imageQuality,
    })
    .fetch();

    if((inputs.inputType=="only-one"||inputs.inputType=="multiple")&&(inputs.optionOrigin='manual'&&inputs.selectOptions.length>0)){
      for(var i=inputs.selectOptions.length-1;i>=0;i--){
        inputs.selectOptions[i].input=newInputRecord.id;
      }
      await SelectOption.createEach(inputs.selectOptions);
    }

    // Since everything went ok, send our 200 response.
    return exits.success({
      input:newInputRecord,
      form:inputs.form
    });

  }

};
