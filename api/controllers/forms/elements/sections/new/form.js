module.exports = {

  friendlyName: 'New section',

  inputs: {
    id:{
      type:'string',
      required:true
    }
  },

  exits: {

  },

  fn: async function(inputs,exits){
    var form=await Form.findOne({
      id:inputs.id
    });
    return this.res.view('pages/forms/elements/sections/new',{
      form:form,
    });
  }

};
