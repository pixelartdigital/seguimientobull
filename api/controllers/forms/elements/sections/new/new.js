module.exports = {

  friendlyName: 'Create new form section',

  inputs: {

    description: {
      type: 'string'
    },

    name:  {
      type: 'string'
    },

    form: {
      required: true,
      type: 'number'
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    // Build up data for the new user record and save it to the database.
    // (Also use `fetch` to retrieve the new ID so that we can use it below.)
    var nextOrder=1;
    var maxbodySection= await BodySection.find({
      select:['order'],
      limit: 1,
      sort: 'order DESC',
      where:{
        form:inputs.form
      }
    });
    if(maxbodySection.length>0){
      nextOrder=maxbodySection[0].order+1;
    }
    var newSectionRecord = await BodySection.create({
      name: inputs.name,
      description: inputs.description,
      form: inputs.form,
      order: nextOrder,
    })
    .fetch();

    // Since everything went ok, send our 200 response.
    return exits.success(newSectionRecord);

  }

};
