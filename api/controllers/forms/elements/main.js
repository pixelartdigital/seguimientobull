module.exports = {

  friendlyName: 'Edit elements',

  inputs: {
    id:{
      type:'string',
      required:true
    }
  },


  exits: {

  },

  fn: async function(inputs,exits){
    var form=await Form.findOne({
      id:inputs.id
    }).populate('bodySections');

    for (var i = form.bodySections.length - 1; i >= 0; i--) {
      form.bodySections[i].inputs=await BodySectionInput.find({
        sort:'order ASC',
        where:{
          bodySection:form.bodySections[i].id
        }
      }).populate('values');
    }

    return this.res.view('pages/forms/elements/main',{
      form:form
    });
  }

};
