module.exports = {

  friendlyName: 'Edit form footer',

  inputs: {

    id:  {
      required: true,
      type: 'string'
    },

    footer: {
      type: 'string'
    }

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var editFormRecord = await Form.update({
      id:inputs.id
    },{
      footer: inputs.footer,
    })
    .fetch();

    // Since everything went ok, send our 200 response.
    return exits.success(editFormRecord);

  }

};
