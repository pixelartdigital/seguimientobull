module.exports = {

  friendlyName: 'Edit form header',

  inputs: {

    id:  {
      required: true,
      type: 'string'
    },

    header: {
      type: 'string'
    }

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var editFormRecord = await Form.update({
      id:inputs.id
    },{
      header: inputs.header,
    })
    .fetch();

    // Since everything went ok, send our 200 response.
    return exits.success(editFormRecord);

  }

};
