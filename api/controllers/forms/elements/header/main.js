module.exports = {

  friendlyName: 'Edit elements',

  inputs: {
    id:{
      type:'string',
      required:true
    }
  },

  exits: {

  },

  fn: async function(inputs,exits){
    var form=await Form.findOne({
      id:inputs.id
    });
    return this.res.view('pages/forms/elements/header',{
      form:form
    });
  }

};
