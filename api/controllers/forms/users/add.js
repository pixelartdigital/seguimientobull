module.exports = {

  friendlyName: 'add user to form',

  inputs: {

    form: {
      type: 'number',
      required: true,
    },

    user:  {
      required: true,
      type: 'number'
    }

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    await Form.addToCollection(inputs.form,'users',inputs.user);
    
    return exits.success();

  }

};
