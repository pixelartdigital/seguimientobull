module.exports = {

  friendlyName: 'remove user from form',

  inputs: {

    form: {
      type: 'number',
      required: true,
    },

    user:  {
      required: true,
      type: 'number'
    }

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    await Form.removeFromCollection(inputs.form,'users',inputs.user);
    
    return exits.success();

  }

};
