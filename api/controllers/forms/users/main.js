module.exports = {

    friendlyName: 'Edit form users',
  
    inputs: {
      id:{
        type:'string',
        required:true
      }
    },
  
  
    exits: {
  
    },
  
    fn: async function(inputs,exits){
      var form=await Form.findOne({
        id:inputs.id,
      }).populate('users');

      var usersId=[];

      for(var i=0;i<form.users.length;i++){
        usersId[i]=form.users[i].id;
      }

      var users=await User.find({
        id: { '!=' : usersId },
      });
  
      return this.res.view('pages/forms/users/main',{
        form:form,
        users:users,
      });
    }
  
  };
  