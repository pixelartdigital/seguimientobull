module.exports = {

  friendlyName: 'Receive a form answer',

  inputs: {

    form:  {
      type: {}
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newRecord=await Record.create().fetch();

    for(var bodPos=0;bodPos<inputs.form.bodySections.length;bodPos++){
      for(var inPos=0;inPos<inputs.form.bodySections[bodPos].inputs.length;inPos++){
        await BodySectionInput.create({
          value:inputs.form.bodySections[bodPos].inputs[inPos].answer,
          input:inputs.form.bodySections[bodPos].inputs[inPos].id,
          record:newRecord.id,
        });
      }
    }

    return exits.success(newRecord);

  }

};
