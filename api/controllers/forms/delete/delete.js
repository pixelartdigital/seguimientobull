module.exports = {

  friendlyName: 'Delete form',

  inputs: {

    form:  {
      required: true,
      type: 'string'
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var bodySections=await BodySection.find({
      form:inputs.form
    });

    var routeGroups=await RouteGroup.find({
      form:inputs.form
    }).populate("routes");

    for(var bsPos=0;bsPos<bodySections.length;bsPos++){
      await BodySectionInput.destroy({
        bodySection:bodySections[bsPos].id
      });
    }
    await BodySection.destroy({
      form:inputs.form
    });

    for(var rgPos=0;rgPos<routeGroups.length;rgPos++){
      for(var roPos=0;roPos<routeGroups[rgPos].routes.length;roPos++){
        await RoutePlace.destroy({
          route:routeGroups[rgPos].routes[roPos].id
        });
      }
      await Route.destroy({
        routeGroup:routeGroups[rgPos].id
      });
    }
    await RouteGroup.destroy({
      form:inputs.form
    });

    await Form.destroy({
      id:inputs.form
    });

    // Since everything went ok, send our 200 response.
    return exits.success();

  }

};
