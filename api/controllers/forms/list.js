var timestampToFormattedDate=function(timestamp){
  var a = new Date(timestamp);
  var months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours() <10 ? '0' + a.getHours() : a.getHours();
  var min = a.getMinutes() < 10 ? '0' + a.getMinutes() : a.getMinutes();
  var sec = a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}

module.exports = {


  friendlyName: 'Forms',


  description: 'Lists the forms.',


  inputs: {

    name: {
      description: 'A part of a name to filter the list.',
      type: 'string'
    },

  },


  exits: {

    success: {
      description: 'The requesting user agent has been successfully logged in.',
      extendedDescription:
`Under the covers, this stores the id of the logged-in user in the session
as the \`userId\` key.  The next time this user agent sends a request, assuming
it includes a cookie (like a web browser), Sails will automatically make this
user id available as req.session.userId in the corresponding action.  (Also note
that, thanks to the included "custom" hook, when a relevant request is received
from a logged-in user, that user's entire record from the database will be fetched
and exposed as \`req.me\`.)`
    },

  },

  fn: async function (inputs, exits) {

    var forms=await Form.find();

    for (var i = forms.length - 1; i >= 0; i--) {
      forms[i].updatedAtDate=timestampToFormattedDate(forms[i].updatedAt);
    }

    return this.res.view('pages/forms/list',{
      forms:forms
    });

  }

};
