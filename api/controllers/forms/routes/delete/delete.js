module.exports = {

  friendlyName: 'Delete route group',

  inputs: {

    routeGroup:  {
      required: true,
      type: 'string'
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var routeGroup=await RouteGroup.findOne({
      id:inputs.routeGroup
    }).populate("routes");

    for(var roPos=0;roPos<routeGroup.routes.length;roPos++){
      await RoutePlace.destroy({
        route:routeGroup.routes[roPos].id
      });
    }
    await Route.destroy({
      routeGroup:inputs.routeGroup
    });
    await RouteGroup.destroy({
      id:inputs.routeGroup
    });

    // Since everything went ok, send our 200 response.
    return exits.success();

  }

};
