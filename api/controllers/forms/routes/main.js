module.exports = {

  friendlyName: 'Edit form routes',

  inputs: {
    id:{
      type:'string',
      required:true
    }
  },


  exits: {

  },

  fn: async function(inputs,exits){
    var form=await Form.findOne({
      id:inputs.id
    }).populate('routeGroups');

    for(var roGr=0;roGr<form.routeGroups.length;roGr++){
      form.routeGroups[roGr].routes=await Route.find({
        routeGroup:form.routeGroups[roGr].id
      });
      for(var ro=0;ro<form.routeGroups[roGr].routes.length;ro++){
        if(form.routeGroups[roGr].routes[ro].user){
          form.routeGroups[roGr].routes[ro].userEmail=await User.findOne({
            id:form.routeGroups[roGr].routes[ro].user
          }).emailAddress;
        }else{
          form.routeGroups[roGr].routes[ro].userEmail='';
        }
      }
    }

    

    return this.res.view('pages/forms/routes/list',{
      form:form,
    });
  }

};
