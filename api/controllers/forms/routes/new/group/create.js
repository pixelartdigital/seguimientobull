module.exports = {

  friendlyName: 'Create new form route group',

  inputs: {

    description: {
      type: 'string'
    },

    name:  {
      required: true,
      type: 'string'
    },

    form:  {
      required: true,
      type: 'string'
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newRouteGroup = await RouteGroup.create({
      name: inputs.name,
      form:inputs.form,
      description: inputs.description,
    })
    .fetch();

    // Since everything went ok, send our 200 response.
    return exits.success(newRouteGroup);

  }

};
