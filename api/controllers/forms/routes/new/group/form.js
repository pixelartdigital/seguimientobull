module.exports = {

  friendlyName: 'show create form route',

  inputs: {
    form:{
      type:'string',
      required:true,
    }
  },


  exits: {

  },

  fn: async function(inputs,exits){
    return this.res.view('pages/forms/routes/newGroup',{
      form:inputs.form,
    });
  }

};
