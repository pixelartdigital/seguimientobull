module.exports = {

  friendlyName: 'save the temp places of a route group',

  inputs: {

    csvList: {
      required: true,
      type: [{}]
    },

    routeGroup:  {
      required: true,
      type: 'string'
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    await TempGeo.destroy({
      routeGroup:inputs.routeGroup,
    });

    for(var rg=0;rg<inputs.csvList.length;rg++){
      if(inputs.csvList[rg].selected){
        inputs.csvList.splice(rg,1);
        rg--;
        continue;
      }
      if(inputs.csvList[rg].id){
        delete inputs.csvList[rg].id;
      }
      inputs.csvList[rg].routeGroup=inputs.routeGroup;
      inputs.csvList[rg].coordenadas=inputs.csvList[rg].coordenadas.lat+','+inputs.csvList[rg].coordenadas.lng;
    }
    await TempGeo.createEach(inputs.csvList);

    // Since everything went ok, send our 200 response.
    return exits.success();

  }

};
