module.exports = {

  friendlyName: 'show create form route',

  inputs: {
    form:{
      type:'string',
      required:true,
    },
    routeGroup:{
      type:'string',
      required:true,
    }
  },


  exits: {

  },

  fn: async function(inputs,exits){
    var routeGroup=await RouteGroup.findOne({
      id:inputs.routeGroup,
    }).populate('tempGeo');
    routeGroup.routes=await Route.find({
      routeGroup:routeGroup.id,
    }).populate("places");
    for(var rt=0;rt<routeGroup.routes.length;rt++){
      if(routeGroup.routes[rt].user){
        routeGroup.routes[rt].user=await User.findOne({
          id:routeGroup.routes[rt].user,
        });
      }else{
        routeGroup.routes[rt].user={
          emailAddress:'',
        };
      }
      for(var rp=0;rp<routeGroup.routes[rt].places.length;rp++){
        routeGroup.routes[rt].places[rp].direccion=routeGroup.routes[rt].places[rp].address;
        routeGroup.routes[rt].places[rp].nombre=routeGroup.routes[rt].places[rp].name;
        routeGroup.routes[rt].places[rp].neighborhood=await Neighborhood.findOne({
          id:routeGroup.routes[rt].places[rp].neighborhood,
        }).populate('city');
        routeGroup.routes[rt].places[rp].neighborhood.city.state=await State.findOne({
          id:routeGroup.routes[rt].places[rp].neighborhood.city.state
        }).populate('country');
        routeGroup.routes[rt].places[rp].barrio=routeGroup.routes[rt].places[rp].neighborhood.name;
        routeGroup.routes[rt].places[rp].ciudad=routeGroup.routes[rt].places[rp].neighborhood.city.name;
        routeGroup.routes[rt].places[rp].departamento=routeGroup.routes[rt].places[rp].neighborhood.city.state.name;
        routeGroup.routes[rt].places[rp].pais=routeGroup.routes[rt].places[rp].neighborhood.city.state.country.name;
      }
    }
    var countries=await Country.find().populate("states");
    for(var co=0;co<countries.length;co++){
      for(var st=0;st<countries[co].states.length;st++){
        countries[co].states[st].cities=await City.find({
          state:countries[co].states[st].id
        }).populate("neighborhoods");
      }
    }
    var users=await User.find({
      role:3
    });
    return this.res.view('pages/forms/routes/new',{
      countries:countries,
      users:users,
      form:inputs.form,
      routeGroup:routeGroup,
    });
  }

};
