module.exports = {

  friendlyName: 'Create new form route',

  inputs: {

    routeGroup: {
      type: 'string',
      required: true,
    },

    route:  {
      type: {}
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newRoute=await Route.create({
      name:inputs.route.name,
      description:inputs.route.description,
      user:inputs.route.user.id,
      routeGroup:inputs.routeGroup,
      day:inputs.route.day,
    })
    .fetch();
    for(var n=0;n<inputs.route.places.length;n++){
      await RoutePlace.create({
        route:newRoute.id,
        name:inputs.route.places[n].nombre,
        address:inputs.route.places[n].direccion,
        coordinates:inputs.route.places[n].coordenadas.lat+','+inputs.route.places[n].coordenadas.lng,
        neighborhood:inputs.route.places[n].neighborhoodID,
      });
    }

    // Since everything went ok, send our 200 response.
    return exits.success(newRoute);

  }

};
