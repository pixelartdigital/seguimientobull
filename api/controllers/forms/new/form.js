module.exports = {

  friendlyName: 'show create form',

  inputs: {

  },


  exits: {

  },

  fn: async function(inputs,exits){
    return this.res.view('pages/forms/new');
  }

};
