module.exports = {

  friendlyName: 'Create new form',

  inputs: {

    description: {
      type: 'string'
    },

    name:  {
      required: true,
      type: 'string'
    }

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    // Build up data for the new user record and save it to the database.
    // (Also use `fetch` to retrieve the new ID so that we can use it below.)
    var newFormRecord = await Form.create({
      name: inputs.name,
      description: inputs.description,
    })
    .fetch();

    // Since everything went ok, send our 200 response.
    return exits.success(newFormRecord);

  }

};
