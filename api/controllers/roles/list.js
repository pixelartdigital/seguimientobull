module.exports = {


  friendlyName: 'Users',


  description: 'Lists the users.',


  inputs: {

  },


  exits: {

  },

  fn: async function (inputs, exits) {

    var roles=await Role.find();

    return this.res.view('pages/roles/list',{
      roles:roles
    });

  }

};
