module.exports = {


  friendlyName: 'Roles per user',

  inputs: {

    id: {
      type: 'string',
      required:true,
    },

  },


  exits: {

  },

  fn: async function (inputs, exits) {

    var role=await Role.findOne({
      id:inputs.id
    }).populate("permissions");

    var permissionsId=[];

    for(var i=0;i<role.permissions.length;i++){
      permissionsId[i]=role.permissions[i].id;
    }

    var permissions=await Permission.find({
      id: { '!=' : permissionsId },
    });

    return this.res.view('pages/roles/privileges/list',{
      role:role,
      permissions:permissions,
    });

  }

};
