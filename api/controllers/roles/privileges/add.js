module.exports = {

  friendlyName: 'add permission to role',

  inputs: {

    role: {
      type: 'number',
      required: true,
    },

    permission:  {
      required: true,
      type: 'number'
    }

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    await Role.addToCollection(inputs.role,'permissions',inputs.permission);
    
    return exits.success();

  }

};
