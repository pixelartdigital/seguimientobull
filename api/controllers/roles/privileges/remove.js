module.exports = {

  friendlyName: 'remove permission from role',

  inputs: {

    role: {
      type: 'number',
      required: true,
    },

    permission:  {
      required: true,
      type: 'number'
    }

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    await Role.removeFromCollection(inputs.role,'permissions',inputs.permission);
    
    return exits.success();

  }

};
