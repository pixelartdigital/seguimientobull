module.exports = {

  friendlyName: 'The forms of a user',

  inputs: {
  },


  exits: {
  },

  fn: async function(inputs,exits){

    var routes=await Route.find({
      user:this.req.session.userId
    }).populate('routeGroup');

    var forms=await Form.find({
      isActive:true,
    }).populate('users');

    var usrExists;

    for(var uf=0;uf<forms.length;uf++){
      usrExists=false;
      for(var usr=0;usr<forms[uf].users.length;usr++){
        if(forms[uf].users[usr].id==this.req.session.userId){
          usrExists=true;
        }
      }
      if(usrExists){
        forms[uf].bodySections=await BodySection.find({
          form:forms[uf].id
        });
      }else{
        forms[uf].deleteMe=true;
      }
    }

    for(var uf=0;uf<forms.length;uf++){
      if(forms[uf].deleteMe){
        forms.splice(uf,1);
        uf=-1;
      }
    }

    var placesIds;
    var visitedPlaces;

    for(var ro=0;ro<routes.length;ro++){
      routes[ro].places=await RoutePlace.find({
        route:routes[ro].id
      }).populate('neighborhood');
      placesIds=[];
      for(var pc=0;pc<routes[ro].places.length;pc++){
        placesIds.push(routes[ro].places[pc].id);
      }
      visitedPlaces=await Record.find({
        place:placesIds,
      }).populate('place');
      while(visitedPlaces.length>0){
        for(var pc=0;pc<routes[ro].places.length;pc++){
          if(routes[ro].places[pc].id==visitedPlaces[0].place.id){
            routes[ro].places.splice(pc,1);
            visitedPlaces.splice(0,1);
            break;
          }
        }
      }
      var routeGroup=routes[ro].routeGroup;
      routeGroup.routes=[routes[ro]];
      routes[ro].routeGroup=routeGroup.id;
      var form= await Form.findOne({
        isActive:true,
        id:routeGroup.form
      }).populate('bodySections');
      if(form){
        forms.push(form);
        form.routeGroups=[routeGroup];
        routeGroup.form=form.id;
      }
    }

    for(var fo=0;fo<forms.length;fo++){
      var bodySections=forms[fo].bodySections;
      for(var bs=0;bs<bodySections.length;bs++){
        bodySections[bs].inputs=await BodySectionInput.find({
          bodySection:bodySections[bs].id
        }).populate('values');
        for(var ip=0;ip<bodySections[bs].inputs.length;ip++){
          if(bodySections[bs].inputs[ip].domain){
            bodySections[bs].inputs[ip].domain=await Domain.findOne({
              id:bodySections[bs].inputs[ip].domain
            }).populate('values');
          }
        }
      }
    }
    return this.res.json(forms);
  }

};
