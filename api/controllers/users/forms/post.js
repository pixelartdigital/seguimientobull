module.exports = {

  friendlyName: 'Get form answers',

  inputs: {

    point:{
      type: {}
    },
    place:{
      type: {}
    },
    form:{
      required: true,
      type: [{}]
    },
    celData:{
      required: true,
      type: {}
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newGeoRecord=await GeoAudit.create({
      date:new Date(inputs.celData.date).getTime(),
      battery:inputs.celData.battery,
      serial:inputs.celData.serial,
      os:inputs.celData.OS,
      lat:inputs.celData.geo.lat,
      lng:inputs.celData.geo.lon,
    })
    .fetch();

    var newRecord = await Record.create({
      form: inputs.form[0].form,
      geo:newGeoRecord.id,
      place:(inputs.point)?inputs.point.id:((inputs.place)?inputs.place.id:null),
      user:this.req.session.userId
    })
    .fetch();

    var bodySection,input;

    for(var bo_in=0;bo_in<inputs.form.length;bo_in++){
      bodySection=inputs.form[bo_in];
      for(var in_in=0;in_in<bodySection.inputs.length;in_in++){
        input=bodySection.inputs[in_in]
        await Answer.create({
          value:(input.answer)?input.answer:"",
          input:input.id,
          record:newRecord.id,
        });
      }
    }

    return exits.success(newRecord);

  }

};
