module.exports = {

  friendlyName: 'show create user',

  inputs: {

  },


  exits: {

  },

  fn: async function(inputs,exits){
    var roles=await Role.find();
    return this.res.view('pages/users/new',{
      roles:roles
    });
  }

};
