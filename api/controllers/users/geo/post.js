module.exports = {

  friendlyName: 'Get form answers',

  inputs: {

    date:{
      type:'string',
      required:true,
    },
    battery:{
      type:'number',
      required:true,
    },
    serial:{
      type:'string',
      required:true,
    },
    OS:{
      type:'string',
      required:true,
    },
    geo:{
      type:{},
      required:true,
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newGeoRecord=await GeoAudit.create({
      date:new Date(inputs.date).getTime(),
      battery:inputs.battery,
      serial:inputs.serial,
      os:inputs.OS,
      lat:inputs.geo.lat,
      lng:inputs.geo.lon,
      user:this.req.session.userId,
    });

    return exits.success(newGeoRecord);

  }

};
