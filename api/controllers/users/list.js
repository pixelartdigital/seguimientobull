var timestampToFormattedDate=function(timestamp){
  var a = new Date(timestamp);
  var months = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours() <10 ? '0' + a.getHours() : a.getHours();
  var min = a.getMinutes() < 10 ? '0' + a.getMinutes() : a.getMinutes();
  var sec = a.getSeconds() < 10 ? '0' + a.getSeconds() : a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}

module.exports = {


  friendlyName: 'Users',


  description: 'Lists the users.',


  inputs: {

    name: {
      description: 'A part of a name to filter the list.',
      type: 'string'
    },

  },


  exits: {

  },

  fn: async function (inputs, exits) {
    var users=await User.find();

    for (var i = users.length - 1; i >= 0; i--) {
      users[i].lastSeenAtDate=timestampToFormattedDate(users[i].lastSeenAt);
    }

    return this.res.view('pages/users/list',{
      users:users
    });

  }

};
