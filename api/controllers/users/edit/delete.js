module.exports = {

  friendlyName: 'Create new user',

  inputs: {

    user: {
      required: true,
      type: 'string',
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    await User.destroy({
      id:inputs.user
    });

    // Since everything went ok, send our 200 response.
    return exits.success();

  }

};
