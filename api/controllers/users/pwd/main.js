module.exports = {

  friendlyName: 'Edit user password',

  inputs: {
    id:{
      type:'string',
      required:true
    }
  },


  exits: {

  },

  fn: async function(inputs,exits){
    var user=await User.findOne({
      id:inputs.id
    });
    var roles=await Role.find();
    return this.res.view('pages/account/edit-password',{
      user:user,
      roles:roles,
    });
  }

};
