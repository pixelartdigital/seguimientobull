module.exports = {

  friendlyName: 'Create new state',

  inputs: {

    country: {
      required: true,
      type: 'string'
    },

    state:  {
      required: true,
      type: {}
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newRecord = await State.createNested({
      state: inputs.state,
      country:inputs.country,
    });

    // Since everything went ok, send our 200 response.
    return exits.success(newRecord);

  }

};
