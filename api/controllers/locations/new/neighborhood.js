module.exports = {

  friendlyName: 'Create new neighborhood',

  inputs: {

    city: {
      required: true,
      type: 'string'
    },

    neighborhood:  {
      required: true,
      type: 'string'
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newRecord = await Neighborhood.create({
      name: inputs.neighborhood,
      city:inputs.city,
    })
    .fetch();

    // Since everything went ok, send our 200 response.
    return exits.success(newRecord);

  }

};
