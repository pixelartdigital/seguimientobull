module.exports = {

  friendlyName: 'Create new city',

  inputs: {

    state: {
      required: true,
      type: 'string'
    },

    city:  {
      required: true,
      type: {}
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newRecord = await City.createNested({
      city: inputs.city,
      state:inputs.state,
    });

    // Since everything went ok, send our 200 response.
    return exits.success(newRecord);

  }

};
