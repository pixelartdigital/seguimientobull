module.exports = {

  friendlyName: 'Create new country',

  inputs: {

    country:  {
      required: true,
      type: {}
    },

  },


  exits: {

  },


  fn: async function (inputs, exits) {

    var newRecord = await Country.createNested({
      country:inputs.country,
    });

    // Since everything went ok, send our 200 response.
    return exits.success(newRecord);

  }

};
