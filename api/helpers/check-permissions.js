module.exports = {


  friendlyName: 'Check permissions',


  description: 'Look up a user\'s "rights".',


  inputs: {
    userId: { type: 'number', required: true }
  },

  exits: {
    success: {
      outputFriendlyName: 'Rights',
      outputDescription: `A user's "rights" within an org.`,
      outputType: ['string']
    },
    orgNotFound: {
      description: 'No such organization exists.'
    }
  },

  fn: async function(inputs, exits) {
    var usr= await User.findOne({
      id:inputs.userId
    });
    var rights = await Role.find({
      id:usr.role
    }).populate("permissions");
    var permissions={};
    if(rights.permissions){
      for(per=0;per<rights.permissions.length;per++){
        permissions[rights.permissions[per]].name=true;
      }
    }
    return exits.success(rights.permissions);
  },

};