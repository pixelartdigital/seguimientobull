parasails.registerPage('listUsers', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    // Form data
    formData: { /* … */ },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* … */ },

    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: '',

    // Success state when form has been submitted
    cloudSuccess: false,

    sortColumnsBy:{
      fullName:'-',
      emailAddress:'-',
      isActive:'-',
      isSuperAdmin:'-',
      lastSeenAt:'-',
    },

    imgSortBoth:'/images/sort_up_down.svg',
    imgSortAsc:'/images/sort_down.svg',
    imgSortDesc:'/images/sort_up.svg',

    imgSortBy:{
      fullName:'/images/sort_up_down.svg',
      emailAddress:'/images/sort_up_down.svg',
      isActive:'/images/sort_up_down.svg',
      isSuperAdmin:'/images/sort_up_down.svg',
      lastSeenAt:'/images/sort_up_down.svg',
    },
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    sortTableBy:function(column){
      var sort;
      for(var sortColumn in this.sortColumnsBy){
        if(sortColumn!=column){
          this.imgSortBy[sortColumn]=this.imgSortBoth;
          this.sortColumnsBy[sortColumn]='-';
        }
      }
      switch(this.sortColumnsBy[column]){
        case '-':
        case 'desc':
          this.imgSortBy[column]=this.imgSortAsc;
          sort=this.sortColumnsBy[column]='asc';
          break;
        case 'asc':
          this.imgSortBy[column]=this.imgSortDesc;
          sort=this.sortColumnsBy[column]='desc';
          break;
      }
      this.users.sort(function(a,b){
        if(a[column]<b[column]){
          if(sort=='asc'){
            return -1;
          }else{
            return 1;
          }
        }
        if(a[column]>b[column]){
          if(sort=='asc'){
            return 1;
          }else{
            return -1;
          }
        }
        return 0;
      });
    },

    deleteUser:function(user){
      axios.post('/api/v1/users/delete', {
        user:user,
        _csrf:this._csrf,
      });
      setTimeout(function(){
        window.location="/users";
      },500);
    },

  }
});
