var Money=VMoney.Money;

parasails.registerPage('editFormElements', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝

  components: {
    Money,
    'datepicker':vuejsDatepicker,
    //'vue-timepicker':VueTimepicker,
  },

  data: {
    // Form data
    formData: { /* … */ },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* … */ },

    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: '',

    // Success state when form has been submitted
    cloudSuccess: false,

    price:0,

    date:'',

    money: {
      decimal: ',',
      thousands: '.',
      prefix: '$ ',
      suffix: '',
      precision: 0,
      masked: false
    },
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
    this.formData=this.form;
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    submittedForm: async function(response) {
      // Otherwise, redirect to the logged-in dashboard.
      // > (Note that we re-enable the syncing state here.  This is on purpose--
      // > to make sure the spinner stays there until the page navigation finishes.)
      this.syncing = true;
      window.location = '/forms/edit/'+this.formData.id;
    },

    handleParsingForm: function() {
      // Clear out any pre-existing error messages.
      this.formErrors = {};

      var argins = this.formData;

      // Validate label:
      if(!argins.label) {
        this.formErrors.label = true;
      }

      // Validate id:
      if(!argins.id) {
        this.formErrors.id = true;
      }

      if(!argins.description) {
        argins.description='';
      }

      if(!argins.newDomainName) {
        argins.newDomainName='';
      }

      if(!argins.newDomainDescription) {
        argins.newDomainDescription='';
      }

      // If there were any issues, they've already now been communicated to the user,
      // so simply return undefined.  (This signifies that the submission should be
      // cancelled.)
      if (Object.keys(this.formErrors).length > 0) {
        return;
      }

      return argins;
    },

    moveUp:function(bodySectionIndex,inputIndex){      
      var bodySection=this.formData.bodySections[bodySectionIndex];
      var input=bodySection.inputs[inputIndex];
      var otherInput=bodySection.inputs[inputIndex-1];
      input.order--;
      otherInput.order++;
      bodySection.inputs.splice(inputIndex-1, 2, input, otherInput);
      axios.post('/api/v1/forms/elements/sections/inputs/reorder', {
        modifiedInputs:[input,otherInput],
        _csrf:this._csrf,
      });
    },
    moveDown:function(bodySectionIndex,inputIndex){
      var bodySection=this.formData.bodySections[bodySectionIndex];
      var input=bodySection.inputs[inputIndex];
      var otherInput=bodySection.inputs[inputIndex+1];
      input.order++;
      otherInput.order--;
      bodySection.inputs.splice(inputIndex, 2, otherInput, input);
      axios.post('/api/v1/forms/elements/sections/inputs/reorder', {
        modifiedInputs:[input,otherInput],
        _csrf:this._csrf,
      });
    },
    borrar:function(bodySectionIndex,inputIndex){
      var bodySection=this.formData.bodySections[bodySectionIndex];
      var modifiedInputs=[];
      for(ip=inputIndex+1;ip<bodySection.inputs.length;ip++){
        bodySection.inputs[ip].order--;
        modifiedInputs.push(bodySection.inputs[ip]);
      }
      axios.post('/api/v1/forms/elements/sections/inputs/delete', {
        deletedInput:bodySection.inputs[inputIndex].id,
        modifiedInputs:modifiedInputs,
        _csrf:this._csrf,
      });
      bodySection.inputs.splice(inputIndex, 1);
    }

  }
});
