parasails.registerPage('newFormSectionInput', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    // Form data
    formData: {
      selectType:'radio',
      optionOrigin:'manual',
      selectOptions:[
        {
          key:'',
          value:'',
        },
      ],
      canTypeOther:false,
      newDomainOptions:[
        {
          key:'',
          value:'',
          description:'',
        },
      ],
      domain:-1,
      yesText:'Si',
      noText:'No',
      minStep:0,
      maxStep:10,
      stepValue:1,
      photoSource:'both',
      imageQuality:40,
    },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* … */ },

    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: '',

    // Success state when form has been submitted
    cloudSuccess: false,
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
    this.formData.form=this.form;
    this.formData.bodySection=this.bodySection;
    this.formData.inputType=this.inputType;
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    submittedForm: async function(response) {
      // Otherwise, redirect to the logged-in dashboard.
      // > (Note that we re-enable the syncing state here.  This is on purpose--
      // > to make sure the spinner stays there until the page navigation finishes.)
      this.syncing = true;
      window.location = '/forms/elements/'+response.form;
    },

    handleParsingForm: function() {
      // Clear out any pre-existing error messages.
      this.formErrors = {};

      var argins = this.formData;

      // Validate full name:
      if(!argins.label) {
        argins.label='';
      }

      if(!argins.help) {
        argins.help='';
      }

      // If there were any issues, they've already now been communicated to the user,
      // so simply return undefined.  (This signifies that the submission should be
      // cancelled.)
      if (Object.keys(this.formErrors).length > 0) {
        return;
      }

      return argins;
    },

    addSelectOption:function(){
      this.formData.selectOptions.push({
        key:'',
        value:''
      });
    },

    removeSelectOption:function(index){
      this.formData.selectOptions.splice(index,1);
    },

    addDomainOption:function(){
      this.formData.newDomainOptions.push({
        key:'',
        value:''
      });
    },

    removeDomainOption:function(index){
      this.formData.newDomainOptions.splice(index,1);
    },

  }
});
