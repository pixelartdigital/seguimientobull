parasails.registerPage('listRoutes', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    // Form data
    formData: { /* … */ },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* … */ },

    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: '',

    // Success state when form has been submitted
    cloudSuccess: false,

    sortColumnsBy:{
      id:'-',
      name:'-',
      user:'-',
    },

    imgSortBoth:'/images/sort_up_down.svg',
    imgSortAsc:'/images/sort_down.svg',
    imgSortDesc:'/images/sort_up.svg',

    imgSortBy:{
      id:'/images/sort_up_down.svg',
      name:'/images/sort_up_down.svg',
      user:'/images/sort_up_down.svg',
    },
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    sortTableBy:function(column){
      var sort;
      for(var sortColumn in this.sortColumnsBy){
        if(sortColumn!=column){
          this.imgSortBy[sortColumn]=this.imgSortBoth;
          this.sortColumnsBy[sortColumn]='-';
        }
      }
      switch(this.sortColumnsBy[column]){
        case '-':
        case 'desc':
          this.imgSortBy[column]=this.imgSortAsc;
          sort=this.sortColumnsBy[column]='asc';
          break;
        case 'asc':
          this.imgSortBy[column]=this.imgSortDesc;
          sort=this.sortColumnsBy[column]='desc';
          break;
      }
      this.forms.routes.sort(function(a,b){
        if(a[column]<b[column]){
          if(sort=='asc'){
            return -1;
          }else{
            return 1;
          }
        }
        if(a[column]>b[column]){
          if(sort=='asc'){
            return 1;
          }else{
            return -1;
          }
        }
        return 0;
      });
    },

    deleteRouteGroup:function(id){
      axios.post('/api/v1/forms/routes/delete', {
        routeGroup:id,
        _csrf:this._csrf,
      }).then(function (response) {
        window.location="/forms/routes/"+this.form.id;
      }.bind(this));
    },

  }
});
