parasails.registerPage('newFormRoute', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝

  components: {
    'datepicker':vuejsDatepicker,
  },

  data: {
    // Form data
    formData: { 
      routes:[],
    },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* … */ },

    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: '',

    // Success state when form has been submitted
    cloudSuccess: false,

    map:null,

    shapeOptions:{
      strokeWeight: 1,
      strokeOpacity: 1,
      fillOpacity: 0.2,
      editable: false,
      clickable: false,
      strokeColor: '#3399FF',
      fillColor: '#3399FF'
    },

    drawingManager:null,

    shift_draw:false,

    lastShape:null,

    places:[],

    selectedPlaces:[],

    failedPlaces:[],

    csvList:[],

    newCountries:[],

    newLocations:[],

    currentRoute:"",

    currentRouteDay:"",

    newRoute:{
      user:null,
      description:'',
    },

    bounds:null,

    pointsToProcess:0,

  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
    this.formData.form=this.form;
  },
  mounted: async function() {
    setTimeout(() => {
      var mapEl=document.getElementById('map');
      if(mapEl){
        this.map = new google.maps.Map(mapEl, {
          center: {lat: 4.7112451, lng: -74.0681418},
          zoom: 15,
          disableDefaultUI: true,
          zoomControl: true,
        });
        this.bounds=new google.maps.LatLngBounds();
        drawingManager = new google.maps.drawing.DrawingManager({
          drawingMode: null,
          drawingControlOptions: {drawingModes: [google.maps.drawing.OverlayType.RECTANGLE]},
          rectangleOptions: this.shapeOptions,
          map: this.map
        });
        google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
          if (this.lastShape != null) {
            this.lastShape.setMap(null);
          }
          // cancel drawing mode
          if (this.shift_draw == false) { 
            drawingManager.setDrawingMode(null); 
          }
          this.lastShape = e.overlay;
          this.lastShape.type = e.type;
          if (this.lastShape.type == google.maps.drawing.OverlayType.RECTANGLE) {    
            lastBounds = this.lastShape.getBounds();
            for(var pos=this.places.length-1;pos>=0;pos--){
              if (this.places[pos].map&&lastBounds.contains(this.places[pos].getPosition())) {
                this.selectedPlaces.push(this.csvList[pos]);
                this.csvList[pos].selected=true;
                this.places[pos].setMap(null);
              }
            }
          }
        }.bind(this));        
      }
      if(this.routeGroup.tempGeo&&this.routeGroup.tempGeo.length>0){
        this.restoreGeo(this.routeGroup.tempGeo);
      }
      if(this.routeGroup.routes&&this.routeGroup.routes.length>0){
        this.restoreRoutes(this.routeGroup.routes);
      }
    }, 500);
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    teclaPresionada:function(e){
      if(e.keyCode==16 && this.shift_draw == false){
        this.map.setOptions({draggable: false, disableDoubleClickZoom: true});
        this.shift_draw = true; // enable drawing
        drawingManager.setDrawingMode(google.maps.drawing.OverlayType.RECTANGLE);
      }
    },

    teclaLiberada:function(e){
      if(e.keyCode==16){
        this.map.setOptions({draggable: true, disableDoubleClickZoom: true});
        this.shift_draw = false // disable drawing
        drawingManager.setDrawingMode(null);
      }
    },

    ratonPresionado:function(e){
      if (this.lastShape != undefined) {
        this.lastShape.setMap(null);
      }
    },

    ratonArrastrado:function(e){
      if (this.lastShape != undefined) {
        this.lastShape.setMap(null);
      }
    },

    submittedForm: async function(response) {
      // Otherwise, redirect to the logged-in dashboard.
      // > (Note that we re-enable the syncing state here.  This is on purpose--
      // > to make sure the spinner stays there until the page navigation finishes.)
      this.syncing = true;
      window.location = '/forms/routes/'+this.form;
    },

    handleParsingForm: function() {
      // Clear out any pre-existing error messages.
      this.formErrors = {};
      
      this.formData.selectedPlaces=this.selectedPlaces;

      var argins = this.formData;

      if(!argins.name) {
        this.formErrors.name = true;
      }

      if(!argins.description) {
        argins.description='';
      }

      // If there were any issues, they've already now been communicated to the user,
      // so simply return undefined.  (This signifies that the submission should be
      // cancelled.)
      if (Object.keys(this.formErrors).length > 0) {
        return;
      }

      return argins;
    },

    loadCSV:function(ev){
      const file = ev.target.files[0];
      const reader = new FileReader();
      reader.addEventListener("load", function(event) {
        var textFile = event.target;
        this.readCSV(textFile.result);
      }.bind(this));
      reader.readAsText(file);
    },

    readCSV:function(csv){
      var lines = csv.split(/[\r\n]+/g);
      lines.splice(0, 1);//at least until column selection becomes a thing
      for(var i=0;i<lines.length;i++){
        lines[i]=lines[i].trim();
        if(lines[i]==""){
          continue;
        }
        lines[i]=lines[i].split(";");
        if(lines[i].length==1){
          continue;
        }
        if(lines[i][6]){
          lines[i][6]=lines[i][6].split(",");
          if(lines[i][6].length==1&&lines[i][7]){
            lines[i][6][1]=lines[i][7];
          }
        }else{
          lines[i][6]=[];
        }
        if(lines[i][6]&&lines[i][6].length>1){
          lines[i][6]={
            lat:parseFloat(lines[i][6][0].trim()),
            lng:parseFloat(lines[i][6][1].trim()),
          };
          this.addPlace(lines[i]);
        }else{
          this.pointsToProcess++;
          this.getCoords(lines[i]);
        }
      }
      if(this.pointsToProcess==0){
        this.saveCsv();
      }
    },

    removeMapPlaces:function(){
      this.bounds=new google.maps.LatLngBounds();
      while(this.places.length>0){
        this.places[0].setMap(null);
        this.places.splice(0,1);
      }
      while(this.csvList.length>0){
        this.csvList.splice(0,1);
      }
      this.saveCsv();
    },

    restoreGeo:function(tempGeo){
      for(var tg=0;tg<tempGeo.length;tg++){
        tempGeo[tg].coordenadas=tempGeo[tg].coordenadas.split(",");
        tempGeo[tg].coordenadas={
          lat:parseFloat(tempGeo[tg].coordenadas[0].trim()),
          lng:parseFloat(tempGeo[tg].coordenadas[1].trim()),
        };
        this.addPlace(null,tempGeo[tg]);
      }
    },

    restoreRoutes:function(routes){
      for(pl=0;pl<routes.length;pl++){
        this.formData.routes.push(routes[pl]);
      }
    },

    addPlace:function(line,tempGeo){
      var csvItem;
      if(line){
        this.bounds.extend(new google.maps.LatLng(line[6]) );
        this.places.push(new google.maps.Marker({
          position: line[6],
          map: this.map,
          title: line[4]
        }));
        csvItem={
          indice:this.places.length-1,
          pais:line[0],
          departamento:line[1],
          ciudad:line[2],
          barrio:line[3],
          nombre:line[4],
          direccion:line[5],
          coordenadas:line[6],
        };
      }else{
        this.bounds.extend(new google.maps.LatLng(tempGeo.coordenadas) );
        this.places.push(new google.maps.Marker({
          position: tempGeo.coordenadas,
          map: this.map,
          title: tempGeo.nombre
        }));
        csvItem=tempGeo;
        csvItem.indice=this.places.length-1;
      }
      if(csvItem.pais.trim()==""){
        csvItem.pais='N/A';
      }
      if(csvItem.departamento.trim()==""){
        csvItem.departamento='N/A';
      }
      if(csvItem.ciudad.trim()==""){
        csvItem.ciudad='N/A';
      }
      if(csvItem.barrio.trim()==""){
        csvItem.barrio='N/A';
      }
      this.checkCountry(csvItem);
      this.csvList.push(csvItem);
      this.map.fitBounds(this.bounds);
    },

    saveCsv:function(){
      axios.post('/api/v1/forms/routes/tempGeo', {
        csvList:this.csvList,
        routeGroup:this.routeGroup.id,
        _csrf:this._csrf,
      });
    },

    getCoords:function(line){
      axios.get('https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyDpX9jy4ZXpulj4rKrOOODgFxAI-rxP88E&address='+encodeURIComponent(line[5]+' '+line[2]+', '+line[1]))
        .then(function (response) {
          if(response.data.status=="OK"&&response.data.results.length>0){
            line[6]=response.data.results[0].geometry.location;
            this.addPlace(line);
            this.pointsToProcess--;
          }else{
            this.pointsToProcess--;
            this.failedPlaces.push({
              indice:this.failedPlaces.length,
              pais:line[0],
              departamento:line[1],
              ciudad:line[2],
              barrio:line[3],
              nombre:line[4],
              direccion:line[5],
              coordenadas:line[6],
            });
          }
          if(this.pointsToProcess==0){
            this.saveCsv();
          }
        }.bind(this))
    },

    removeSelectedPlace:function(indice,placeIndex){
      this.places[indice].setMap(this.map);
      this.csvList[indice].selected=false;
      this.selectedPlaces.splice(placeIndex,1);
    },

    removeSelectedPlaces:function(){
      while(this.selectedPlaces.length>0){
        this.places[this.selectedPlaces[0].indice].setMap(this.map);
        this.csvList[this.selectedPlaces[0].indice].selected=false;
        this.selectedPlaces.splice(0,1);
      }
    },

    checkCountry:function(item){
      var countryIndex=-1;
      var newCountryIndex=-1;
      var newCountry;
      for(co=0;co<this.countries.length;co++){
        if(item.pais==this.countries[co].name){
          countryIndex=co;
          break;
        }
      }
      if(countryIndex==-1){
        for(co=0;co<this.newCountries.length;co++){
          if(item.pais==this.newCountries[co].name){
            newCountryIndex=co;
            break;
          }
        }
        if(newCountryIndex==-1){
          newCountry={
            name:item.pais,
            states:[],
          };
          item.noRegistrado=true;
          this.newCountries.push(newCountry);
          this.checkState(item,newCountry);
        }else{
          this.checkState(item,this.newCountries[newCountryIndex]);
        }
      }else{
        item.countryID=this.countries[countryIndex].id;
        this.checkState(item,this.countries[countryIndex]);
      }
      if(item.noRegistrado){
        this.newLocations.push(item);
      }
    },

    checkState: function(item,country){
      var stateIndex=-1;
      var newStateIndex=-1;
      var newState;
      for(co=0;co<country.states.length;co++){
        if(item.departamento==country.states[co].name){
          stateIndex=co;
          break;
        }
      }
      if(stateIndex==-1){
        if(!country.newStates){
          country.newStates=[];
        }
        for(co=0;co<country.newStates.length;co++){
          if(item.departamento==country.newStates[co].name){
            newStateIndex=co;
            break;
          }
        }
        if(newStateIndex==-1){
          newState={
            name:item.departamento,
            cities:[],
          };
          item.noRegistrado=true;
          country.newStates.push(newState);
          this.checkCity(item,newState);
        }else{
          this.checkCity(item,country.newStates[newStateIndex]);
        }
      }else{
        item.stateID=country.states[stateIndex].id;
        this.checkCity(item,country.states[stateIndex]);
      }
    },

    checkCity: function(item,state){
      var cityIndex=-1;
      var newCityIndex=-1;
      var newCity;
      for(co=0;co<state.cities.length;co++){
        if(item.ciudad==state.cities[co].name){
          cityIndex=co;
          break;
        }
      }
      if(cityIndex==-1){
        if(!state.newCities){
          state.newCities=[];
        }
        for(co=0;co<state.newCities.length;co++){
          if(item.ciudad==state.newCities[co].name){
            newCityIndex=co;
            break;
          }
        }
        if(newCityIndex==-1){
          newCity={
            name:item.ciudad,
            neighborhoods:[],
          };
          item.noRegistrado=true;
          state.newCities.push(newCity);
          this.checkNeighborhood(item,newCity);
        }else{
          this.checkNeighborhood(item,state.newCities[newCityIndex]);
        }
      }else{
        item.cityID=state.cities[cityIndex].id;
        this.checkNeighborhood(item,state.cities[cityIndex]);
      }
    },

    checkNeighborhood: function(item,city){
      var neighborhoodIndex=-1;
      var newNeighborhoodIndex=-1;
      var newNeighborhood;
      for(co=0;co<city.neighborhoods.length;co++){
        if(item.barrio==city.neighborhoods[co].name){
          neighborhoodIndex=co;
          break;
        }
      }
      if(neighborhoodIndex==-1){
        if(!city.newNeighborhoods){
          city.newNeighborhoods=[];
        }
        for(co=0;co<city.newNeighborhoods.length;co++){
          if(item.barrio==city.newNeighborhoods[co].name){
            newNeighborhoodIndex=co;
            break;
          }
        }
        if(newNeighborhoodIndex==-1){
          newNeighborhood={
            name:item.barrio,
            neighborhoods:[],
          };
          item.noRegistrado=true;
          city.newNeighborhoods.push(newNeighborhood);
        }
      }
      else
      {
        item.neighborhoodID=city.neighborhoods[neighborhoodIndex].id;
      }
    },

    registrarLocaciones:function(){
      this.syncing=true;
      for(var co=0;co<this.newCountries.length;co++){
        this.registrarPais(this.newCountries[co]);
      }
      this.newCountries.splice(0, this.newCountries.length)
      for(var co=0;co<this.countries.length;co++){
        if(this.countries[co].newStates){
          for(var st=0;st<this.countries[co].newStates.length;st++){
            this.registrarDepartamento(this.countries[co],this.countries[co].newStates[st]);
          }
          this.countries[co].newStates.splice(0, this.countries[co].newStates.length)
        }
        for(var st=0;st<this.countries[co].states.length;st++){
          if(this.countries[co].states[st].newCities){
            for(var ci=0;ci<this.countries[co].states[st].newCities.length;ci++){
              this.registrarCiudad(this.countries[co].states[st],this.countries[co].states[st].newCities[ci]);
            }
            this.countries[co].states[st].newCities.splice(0, this.countries[co].states[st].newCities.length)
          }
          for(var ci=0;ci<this.countries[co].states[st].cities.length;ci++){
            if(this.countries[co].states[st].cities[ci].newNeighborhoods){
              for(var ne=0;ne<this.countries[co].states[st].cities[ci].newNeighborhoods.length;ne++){
                this.registrarBarrio(this.countries[co].states[st].cities[ci],this.countries[co].states[st].cities[ci].newNeighborhoods[ne]);
              }
              this.countries[co].states[st].cities[ci].newNeighborhoods.splice(0, this.countries[co].states[st].cities[ci].newNeighborhoods.length)
            }
          }
        }
      }
      setTimeout(function(){
        for(var csv=0;csv<this.csvList.length;csv++){
          this.checkCountry(this.csvList[csv]);
        }
        this.syncing=false;
      }.bind(this),this.newLocations.length*35);
      this.newLocations.splice(0, this.newLocations.length);
    },

    registrarPais:function(pais){
      axios.post('/api/v1/locations/new/country', {
        country:pais,
        _csrf:this._csrf,
      }).then(function (response) {
        this.countries.push(response);
      }.bind(this));
    },

    registrarDepartamento:function(pais,departamento){
      axios.post('/api/v1/locations/new/state', {
        country:pais.id,
        state:departamento,
        _csrf:this._csrf,
      }).then(function (response) {
        pais.states.push(response);
      }.bind(this));
    },

    registrarCiudad:function(departamento,ciudad){
      axios.post('/api/v1/locations/new/city', {
        state:departamento.id,
        city:ciudad,
        _csrf:this._csrf,
      }).then(function (response) {
        departamento.cities.push(response);
      }.bind(this));
    },

    registrarBarrio:function(ciudad,barrio){
      axios.post('/api/v1/locations/new/neighborhood', {
        city:ciudad.id,
        neighborhood:barrio.name,
        _csrf:this._csrf,
      }).then(function (response) {
        ciudad.neighborhoods.push(response);
      }.bind(this));
    },

    addRoute:function(){
      var date=this.currentRouteDay;
      var route={
        name:this.currentRoute,
        day:date.getFullYear()+'/'+((date.getMonth()+1)<10?'0'+(date.getMonth()+1):date.getMonth()+1)+'/'+(date.getDate()<10?'0'+date.getDate():date.getDate()),
        description:this.newRoute.description,
        user:(this.newRoute.user)?this.users[this.newRoute.user]:{email:''},
        places:this.selectedPlaces,
      };
      axios.post('/api/v1/forms/routes/new', {
        route:route,
        routeGroup:this.routeGroup.id,
        _csrf:this._csrf,
      });
      this.selectedPlaces=[];
      this.newRoute={
        user:null,
        description:'',
      },
      this.newRoute.description='';
      this.formData.routes.push(route);
      this.currentRoute="";
      this.currentRouteDay="";
      this.saveCsv();
    },

  }
});
