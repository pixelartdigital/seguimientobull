parasails.registerPage('welcome', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    dashboardModalVisible: false,
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    setTimeout(() => {
      var map;
      var mapEl=document.getElementById('map');
      if(mapEl){
        map = new google.maps.Map(mapEl, {
          center: {lat: 4.7112451, lng: -74.0681418},
          zoom: 15
        });
        for(var usr=0;usr<this.userAudits.length;usr++){
          for(var pos=0;pos<this.userAudits[usr].geos.length;pos++){
            new google.maps.Marker({
              position: {
                lat:parseFloat(this.userAudits[usr].geos[pos].lat),
                lng:parseFloat(this.userAudits[usr].geos[pos].lng),
              },
              map: map,
              title: this.userAudits[usr].fullName
            })
          }
        }
      }
    }, 500);
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    clickOpenDashboardModalButton: async function() {
      this.dashboardModalVisible = true;
    },

    closeDashboardModal: async function() {
      this.dashboardModalVisible = false;
    },

  }
});