parasails.registerPage('reporter', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝

  components: {
    'datepicker':vuejsDatepicker,
  },

  data: {
    // Form data
    formData: {
    },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* … */ },

    // Syncing / loading state
    syncing: false,

    // Server error state
    cloudError: '',

    // Success state when form has been submitted
    cloudSuccess: false,

    selectedForm:null,

    checkedColumns:[],

    records:[],

    graphDataColumn:null,

    graphLabelColumn:null,

    graphTypeColumn:'bar',

    recomputeTableRecords:0,

  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    // Attach any initial data from the server.
    _.extend(this, SAILS_LOCALS);
  },
  mounted: async function() {
    this.reportGraph=document.getElementById('reportGraph');
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  computed: {

    columns:function(){
      if(!this.selectedForm&&this.selectedForm!==0){
        return [];
      }
      var columns=[
        {
          inputType:'string',
          label:'Barrio (punto)',
          order:-10,
          id:-10,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'Ciudad (punto)',
          order:-11,
          id:-11,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'Departamento (punto)',
          order:-12,
          id:-12,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'País (punto)',
          order:-13,
          id:-13,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'Nombre (punto)',
          order:-9,
          id:-9,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'Dirección (punto)',
          order:-8,
          id:-8,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'Nombre (encuestador)',
          order:-7,
          id:-7,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'E-mail (encuestador)',
          order:-6,
          id:-6,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'date',
          label:'Fecha (dispositivo)',
          order:-5,
          id:-5,
          date:true,
          values:[],
        },
        {
          inputType:'number',
          label:'Batería (dispositivo)',
          order:-4,
          id:-4,
          numeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'Serial (dispositivo)',
          order:-3,
          id:-3,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'string',
          label:'Sistema Operativo (dispositivo)',
          order:-2,
          id:-2,
          alphaNumeric:true,
          values:[],
        },
        {
          inputType:'geolocation',
          label:'Localización (dispositivo)',
          order:-1,
          id:-1,
          alphaNumeric:true,
          values:[],
        },
      ];
      var form=this.forms[this.selectedForm];
      var bodySections=form.bodySections;
      var bodySection;
      var inputs;
      var input;
      if(bodySections){
        for(var body=0;body<bodySections.length;body++){
          bodySection=bodySections[body];
          inputs=bodySection.inputs;
          if(inputs){
            for(var inp=0;inp<inputs.length;inp++){
              input=inputs[inp];
              if(input.includeInFilters){
                columns.push(input);
              }
            }
          }
        }
      }
      axios
        .get('/api/v1/report/records?form='+this.forms[this.selectedForm].id)
        .then(function (response) {
          var dataRecord;
          var date;
          for(var i=0;i<response.data.length;i++){
            dataRecord=response.data[i];
            for(var ansr=0;ansr<dataRecord.answers.length;ansr++){
              for(var cl=0;cl<columns.length;cl++){
                if(columns[cl].id==dataRecord.answers[ansr].input){
                  if(dataRecord.answers[ansr].input.inputType=='date'){
                    date=new Date(dataRecord.answers[ansr].value);
                    dataRecord.answers[ansr].value=date.getFullYear()+'/'+((date.getMonth()+1)<10?'0'+(date.getMonth()+1):date.getMonth()+1)+'/'+(date.getDate()<10?'0'+date.getDate():date.getDate())+' '+(date.getHours()<10?'0'+date.getHours():date.getHours())+':'+(date.getMinutes()<10?'0'+date.getMinutes():date.getMinutes())+':'+(date.getSeconds()<10?'0'+date.getSeconds():date.getSeconds());
                    dataRecord.answers[ansr].innerValue=date;
                  }
                  break;
                }
              }
            }
            date=new Date(dataRecord.geo.date);
            dataRecord.answers.push({
              value:date.getFullYear()+'/'+((date.getMonth()+1)<10?'0'+(date.getMonth()+1):date.getMonth()+1)+'/'+(date.getDate()<10?'0'+date.getDate():date.getDate())+' '+(date.getHours()<10?'0'+date.getHours():date.getHours())+':'+(date.getMinutes()<10?'0'+date.getMinutes():date.getMinutes())+':'+(date.getSeconds()<10?'0'+date.getSeconds():date.getSeconds()),
              innerValue:date,
              input:-5,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.geo.battery,
              input:-4,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.geo.serial,
              input:-3,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.geo.os,
              input:-2,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.geo.lat+','+dataRecord.geo.lng,
              input:-1,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.user.emailAddress,
              input:-6,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.user.fullName,
              input:-7,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.place.address,
              input:-8,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.place.name,
              input:-9,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.place.neighborhood.name,
              input:-10,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.place.neighborhood.city.name,
              input:-11,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.place.neighborhood.city.state.name,
              input:-12,
              record:dataRecord.id,
            });
            dataRecord.answers.push({
              value:dataRecord.place.neighborhood.city.state.country.name,
              input:-13,
              record:dataRecord.id,
            });
          }
          this.records = response.data;
        }.bind(this))
      return columns;
    },

    tableColumns:function(){
      var columns=[];
      for(var i=0;i<this.checkedColumns.length;i++){
        columns.push(this.columns[this.checkedColumns[i]]);
      }
      return columns;
    },

    tableRecords:function(){
      this.recomputeTableRecords;
      var records=[];
      var dataRecord;
      var record;
      var answer;
      var isValid;
      var tableCol;
      var found;
      for(var rec=0;rec<this.records.length;rec++){
        isValid=true;
        dataRecord=this.records[rec];
        record={
          columns:[],
        };
        for(var col=0;col<this.tableColumns.length&&isValid;col++){
          tableCol=this.tableColumns[col];
          found=false;
          for(var inp=0;inp<dataRecord.answers.length&&isValid;inp++){
            answer=dataRecord.answers[inp];
            if(!answer.inputData){
              for(var colTy=0;colTy<this.columns.length;colTy++){
                if(answer.input==this.columns[colTy].id){
                  answer.inputData=this.columns[colTy];
                }
              }
            }
            if(answer.input==tableCol.id){
              found=true;
              record.columns.push(answer);
              switch(tableCol.filterType){
                case 'equals':
                  if(answer.value!=tableCol.filter){
                    isValid=false;
                  }
                  break;
                case 'notEquals':
                  if(answer.value==tableCol.filter){
                    isValid=false;
                  }
                  break;
                case 'contains':
                  if(answer.value.indexOf(tableCol.filter)<0){
                    isValid=false;
                  }
                  break;
                case 'lessThan':
                  if(answer.value>=tableCol.filter){
                    isValid=false;
                  }
                  break;
                case 'before':
                  if(answer.innerValue>=tableCol.filter){
                    isValid=false;
                  }
                  break;
                case 'after':
                  if(answer.innerValue<=tableCol.filter){
                    isValid=false;
                  }
                  break;
                case 'greaterThan':
                  if(answer.value<=tableCol.filter){
                    isValid=false;
                  }
                  break;
                case 'lessOrEqualsThan':
                  if(answer.value>tableCol.filter){
                    isValid=false;
                  }
                  break;
                case 'greaterOrEqualsThan':
                  if(answer.value<tableCol.filter){
                    isValid=false;
                  }
                  break;
                case 'between':
                  if(!tableCol.date){
                    if(answer.value<tableCol.filter||answer.value>tableCol.secondFilter){
                      isValid=false;
                    }
                  }else{
                    if(answer.innerValue<tableCol.filter||answer.innerValue>tableCol.secondFilter){
                      isValid=false;
                    }
                  }
                  break;
              }
            }
          }
        }
        if(isValid){
          records.push(record);
        }
      }
      return records;
    },

    graphDataSelectValues:function(){
      var options=[];
      var column;
      for(var col=0;col<this.tableColumns.length;col++){
        column=this.tableColumns[col];
        if(column.numeric){
          options.push(column);
        }
      }
      if(options.length>0){
        this.drawGraph();
      }
      return options;
    },

  },

  methods:{

    updateRecords:function(){
      this.recomputeTableRecords++;
    },

    drawGraph:function(){

      RGraph.clear(this.reportGraph);

      if(this.records.length==0){
        return;
      }

      var labelColumn;
      var dataColumn;

      for(var col=0;col<this.tableColumns.length;col++){
        if(this.tableColumns[col].id==this.graphLabelColumn){
          labelColumn=this.tableColumns[col];
          break;
        }
      }

      for(var col=0;col<this.tableColumns.length;col++){
        if(this.tableColumns[col].id==this.graphDataColumn){
          dataColumn=this.tableColumns[col];
          break;
        }
      }

      if(!labelColumn||!dataColumn){
        return;
      }

      var dataRecord=this.records[0];

      var valueIndex=0;

      var labelIndex=0;

      for(var inp=0;inp<dataRecord.answers.length;inp++){
        if(dataRecord.answers[inp].input==this.graphDataColumn){
          valueIndex=inp;
        }
        if(dataRecord.answers[inp].input==this.graphLabelColumn){
          labelIndex=inp;
        }
      }

      var dataTmp;
      var labelTmp;

      var data = [];
      var labels=[];
      var labelPos;

      for(var rec=0;rec<this.records.length;rec++){
        dataRecord=this.records[rec];
        if((labelPos=labels.indexOf(dataRecord.answers[labelIndex].value))==-1){
          if(labelColumn.numeric){
            labels.push(parseInt(dataRecord.answers[labelIndex].value));
          }else{
            labels.push(dataRecord.answers[labelIndex].value);
          }
          data.push(parseInt(dataRecord.answers[valueIndex].value,10));
        }else{
          data[labelPos]+=parseInt(dataRecord.answers[valueIndex].value,10);
        }
      }

      if(this.tableColumns[col].numeric){
        for(var count=0;count<(labels.length-1);count++){
          for(var lab=0;lab<(labels.length-1);lab++){
            if(labels[lab]>labels[lab+1]){
              dataTmp=data[lab];
              labelTmp=labels[lab];
              data[lab]=data[lab+1];
              labels[lab]=labels[lab+1];
              data[lab+1]=dataTmp;
              labels[lab+1]=labelTmp;
            }
          }
        }
      }

      switch(this.graphTypeColumn){
        case 'bar':
          new RGraph.Bar({
              id: 'reportGraph',
              data: data,
              options: {
                labels: labels,
              }
          }).draw();
          break;
        case 'line':
          new RGraph.Line({
              id: 'reportGraph',
              data: data,
              options: {
                labels: labels,
              }
          }).draw();
          break;
        case 'hbar':
          new RGraph.HBar({
              id: 'reportGraph',
              data: data,
              options: {
                labels: labels,
              }
          }).draw();
          break;
        case 'pie':
          new RGraph.Pie({
              id: 'reportGraph',
              data: data,
              options: {
                key: labels,
              }
          }).draw();
          break;
        case 'donut':
          new RGraph.Pie({
              id: 'reportGraph',
              data: data,
              options: {
                variant: 'donut',
                labels: labels,
              }
          }).draw();
          break;
        case 'rose':
          new RGraph.Rose({
              id: 'reportGraph',
              data: data,
              options: {
                labels: labels,
              }
          }).draw();
          break;
        case 'radar':
          new RGraph.Radar({
              id: 'reportGraph',
              data: data,
              options: {
                labels: labels,
              }
          }).draw();
          break;
      }
    },

    downloadCSV:function(){
      if(this.tableColumns.length==0){
        return;
      }
      var csv='data:text/csv;charset=utf-8,';
      for(var col=0;col<this.tableColumns.length;col++){
        if(this.tableColumns[col].inputType!='photo'){
          csv+=this.tableColumns[col].label+';';
        }
      }
      csv+='\n';
      for(var rec=0;rec<this.tableRecords.length;rec++){
        dataRecord=this.tableRecords[rec];
        for(var col=0;col<dataRecord.columns.length;col++){
          if(this.tableColumns[col].inputType!='photo'){
            csv+=dataRecord.columns[col].value+';';
          }
        }
        csv+='\n';
      }
      link = document.createElement('a');
      link.setAttribute('href', encodeURI(csv));
      link.setAttribute('download', 'export.csv');
      link.click();
    },

    downloadPDF:function(){
      var columns=[];
      for(var cl=0;cl<this.tableColumns.length;cl++){
        if(this.tableColumns[cl].inputType!='photo'){
          columns.push(this.tableColumns[cl].label);
        }
      }
      var rows=[];
      var row;
      for(var tr=0;tr<this.tableRecords.length;tr++){
        row=[];
        for(var rc=0;rc<this.tableRecords[tr].columns.length;rc++){
          if(this.tableColumns[rc].inputType!='photo'){
            row.push(this.tableRecords[tr].columns[rc].value);
          }
        }
        rows.push(row);
      }
      var doc = new jsPDF('l', 'pt', 'letter');
      doc.autoTable(columns, rows);
      doc.save('export.pdf')
    },

  }
  
});
