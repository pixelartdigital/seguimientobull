/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝
  //home
  'GET /':{
    action: 'dashboard/view-welcome'
  },
  'GET /welcome':{
    action: 'dashboard/view-welcome'
  },
  //static pages
  'GET /faq':                { view:   'pages/faq' },
  'GET /legal/terms':        { view:   'pages/legal/terms' },
  'GET /legal/privacy':      { view:   'pages/legal/privacy' },
  'GET /contact':            { view:   'pages/contact' },

  'GET /signup':             { action: 'entrance/view-signup' },
  'GET /email/confirm':      { action: 'entrance/confirm-email' },
  'GET /email/confirmed':    { view:   'pages/entrance/confirmed-email' },

  'GET /login':              { action: 'entrance/view-login' },
  'GET /password/forgot':    { action: 'entrance/view-forgot-password' },
  'GET /password/new':       { action: 'entrance/view-new-password' },

  'GET /account':            { action: 'account/view-account-overview' },
  'GET /account/password':   { action: 'account/view-edit-password' },
  'GET /account/profile':    { action: 'account/view-edit-profile' },

  '/forms':                  { action: 'forms/list' },
  'GET /forms/new':          { action: 'forms/new/form' },
  'GET /forms/edit/:id':     { action: 'forms/edit/main' },
  'GET /forms/elements/:id':     { action: 'forms/elements/main' },
  'GET /forms/elements/header/:id':     { action: 'forms/elements/header/main' },
  'GET /forms/elements/footer/:id':     { action: 'forms/elements/footer/main' },
  'GET /forms/elements/sections/new/:id':     { action: 'forms/elements/sections/new/form' },
  'GET /forms/elements/sections/edit/:id':     { action: 'forms/elements/sections/edit/form' },
  'GET /forms/elements/sections/inputs/new/:form/:bodySection':     { action: 'forms/elements/sections/inputs/new/form' },
  'GET /forms/elements/sections/inputs/edit/:form/:input':     { action: 'forms/elements/sections/inputs/edit/form' },
  'GET /forms/elements/sections/inputs/new/:form/:bodySection/:inputType':     { action: 'forms/elements/sections/inputs/new/type' },
  'GET /forms/users/:id':     { action: 'forms/users/main' },
  'GET /forms/routes/:id':     { action: 'forms/routes/main' },
  'GET /forms/routes/new/:form':     { action: 'forms/routes/new/group/form' },
  'GET /forms/routes/new/:form/:routeGroup':     { action: 'forms/routes/new/form' },
  'GET /forms/routes/edit/:form/:routeGroup':     { action: 'forms/routes/new/form' },

  '/users':                  { action: 'users/list' },
  'GET /users/new':          { action: 'users/new/form' },
  'GET /users/edit/:id':     { action: 'users/edit/main' },
  'GET /users/pwd/:id':     { action: 'users/pwd/main' },

  '/roles':                  { action: 'roles/list' },
  'GET /roles/new':          { action: 'roles/new/form' },
  'GET /roles/privileges/:id':     { action: 'roles/privileges/main' },
  'GET /privileges/new':     { action: 'privileges/form' },

  '/gallery':                  { action: 'gallery/form' },

  '/report':                  { action: 'report/main' },

  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝
  // Note that, in this app, these API endpoints may be accessed using the `Cloud.*()` methods
  // from the CloudSDK library.
  '/api/v1/account/logout':                              { action: 'account/logout' },
  'PUT   /api/v1/account/update-password':               { action: 'account/update-password' },
  'PUT   /api/v1/account/update-profile':                { action: 'account/update-profile' },
  'PUT   /api/v1/account/update-billing-card':           { action: 'account/update-billing-card' },
  'PUT   /api/v1/entrance/login':                        { action: 'entrance/login' },
  'POST  /api/v1/entrance/signup':                       { action: 'entrance/signup' },
  'POST  /api/v1/entrance/send-password-recovery-email': { action: 'entrance/send-password-recovery-email' },
  'POST  /api/v1/entrance/update-password-and-login':    { action: 'entrance/update-password-and-login' },
  'POST  /api/v1/deliver-contact-form-message':          { action: 'deliver-contact-form-message' },

  'POST  /api/v1/forms/new':                             { action: 'forms/new/create' },
  'POST  /api/v1/forms/answer':                             { action: 'forms/answer/receive' },
  'POST  /api/v1/forms/edit':                             { action: 'forms/edit/edit' },
  'POST  /api/v1/forms/delete':                             { action: 'forms/delete/delete' },
  'POST  /api/v1/forms/elements/header':                      { action: 'forms/elements/header/edit' },
  'POST  /api/v1/forms/elements/footer':                        { action: 'forms/elements/footer/edit' },
  'POST  /api/v1/forms/elements/sections/new':            { action: 'forms/elements/sections/new/new' },
  'POST  /api/v1/forms/elements/sections/inputs/new':{ action: 'forms/elements/sections/inputs/new/new' },
  'POST /api/v1/forms/elements/sections/inputs/edit':     { action: 'forms/elements/sections/inputs/edit/edit' },
  'POST /api/v1/forms/elements/sections/inputs/reorder':     { action: 'forms/elements/sections/inputs/edit/reorder' },
  'POST /api/v1/forms/elements/sections/inputs/delete':     { action: 'forms/elements/sections/inputs/edit/delete' },
  'POST  /api/v1/forms/elements/sections/edit':         { action: 'forms/elements/sections/edit' },
  'POST /api/v1/forms/users/add':     { action: 'forms/users/add' },
  'POST /api/v1/forms/users/remove':     { action: 'forms/users/remove' },
  'POST /api/v1/forms/routes/newGroup':     { action: 'forms/routes/new/group/create' },
  'POST /api/v1/forms/routes/delete':     { action: 'forms/routes/delete/delete' },
  'POST /api/v1/forms/routes/new':     { action: 'forms/routes/new/create' },
  'POST /api/v1/forms/routes/tempGeo':     { action: 'forms/routes/new/tempgeo' },

  'POST  /api/v1/users/new':                             { action: 'users/new/create' },
  'POST  /api/v1/users/edit':                             { action: 'users/edit/edit' },
  'POST  /api/v1/users/delete':                             { action: 'users/edit/delete' },
  'GET  /api/v1/report/records':                             { action: 'report/records' },
  'GET  /api/forms/user':                             { action: 'users/forms/get' },
  'POST  /api/forms/user':                             { action: 'users/forms/post' },
  'POST  /api/user/geo':                             { action: 'users/geo/post' },

  'POST  /api/v1/roles/new':                             { action: 'roles/new/create' },
  'POST /api/v1/roles/privileges/remove':     { action: 'roles/privileges/remove' },
  'POST /api/v1/roles/privileges/add':     { action: 'roles/privileges/add' },
  'POST /api/v1/privileges/new':     { action: 'roles/privileges/create' },

  'POST  /api/v1/locations/new/country':                             { action: 'locations/new/country' },
  'POST  /api/v1/locations/new/state':                             { action: 'locations/new/state' },
  'POST  /api/v1/locations/new/city':                             { action: 'locations/new/city' },
  'POST  /api/v1/locations/new/neighborhood':                             { action: 'locations/new/neighborhood' },

  'GET /api/v1/gallery/get':                  { action: 'gallery/images' },

  "GET /csrfToken": { action: "security/grant-csrf-token" },


  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗  ╦═╗╔═╗╔╦╗╦╦═╗╔═╗╔═╗╔╦╗╔═╗
  //  ║║║║╚═╗║    ╠╦╝║╣  ║║║╠╦╝║╣ ║   ║ ╚═╗
  //  ╩ ╩╩╚═╝╚═╝  ╩╚═╚═╝═╩╝╩╩╚═╚═╝╚═╝ ╩ ╚═╝
  '/terms':                   '/legal/terms',
  '/logout':                  '/api/v1/account/logout',

};
