--
-- PostgreSQL database dump
--

-- Dumped from database version 10.4
-- Dumped by pg_dump version 10.4

-- Started on 2018-08-16 12:35:07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 200 (class 1259 OID 29585)
-- Name: answer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.answer (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    value text,
    input integer,
    record integer
);


ALTER TABLE public.answer OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 29582)
-- Name: answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.answer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.answer_id_seq OWNER TO postgres;

--
-- TOC entry 2960 (class 0 OID 0)
-- Dependencies: 198
-- Name: answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.answer_id_seq OWNED BY public.answer.id;


--
-- TOC entry 223 (class 1259 OID 29713)
-- Name: archive; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.archive (
    id integer NOT NULL,
    "createdAt" bigint,
    "fromModel" text,
    "originalRecord" json,
    "originalRecordId" json
);


ALTER TABLE public.archive OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 29707)
-- Name: archive_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.archive_id_seq OWNER TO postgres;

--
-- TOC entry 2961 (class 0 OID 0)
-- Dependencies: 222
-- Name: archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.archive_id_seq OWNED BY public.archive.id;


--
-- TOC entry 204 (class 1259 OID 29600)
-- Name: bodysection; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bodysection (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    name text,
    description text,
    "order" real,
    form integer,
    "bodySection" integer
);


ALTER TABLE public.bodysection OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 29589)
-- Name: bodysection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bodysection_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bodysection_id_seq OWNER TO postgres;

--
-- TOC entry 2962 (class 0 OID 0)
-- Dependencies: 201
-- Name: bodysection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bodysection_id_seq OWNED BY public.bodysection.id;


--
-- TOC entry 202 (class 1259 OID 29592)
-- Name: bodysectioninput; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.bodysectioninput (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    "inputType" text,
    label text,
    "order" real,
    help text,
    "isRequired" boolean,
    "includeInFilters" boolean,
    "maxLength" real,
    "minLength" real,
    "maxValue" real,
    "minValue" real,
    is12h boolean,
    "selectType" text,
    "canTypeOther" boolean,
    "labelCanTypeOther" text,
    "yesText" text,
    "noText" text,
    "minStep" real,
    "maxStep" real,
    "stepValue" real,
    "editableLocation" boolean,
    "photoSource" text,
    "imageQuality" real,
    domain integer,
    "bodySection" integer
);


ALTER TABLE public.bodysectioninput OWNER TO postgres;

--
-- TOC entry 199 (class 1259 OID 29584)
-- Name: bodysectioninput_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.bodysectioninput_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bodysectioninput_id_seq OWNER TO postgres;

--
-- TOC entry 2963 (class 0 OID 0)
-- Dependencies: 199
-- Name: bodysectioninput_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.bodysectioninput_id_seq OWNED BY public.bodysectioninput.id;


--
-- TOC entry 197 (class 1259 OID 19251)
-- Name: category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.category (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    name text,
    description text
);


ALTER TABLE public.category OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 19244)
-- Name: category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.category_id_seq OWNER TO postgres;

--
-- TOC entry 2964 (class 0 OID 0)
-- Dependencies: 196
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;


--
-- TOC entry 206 (class 1259 OID 29609)
-- Name: city; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.city (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    name text,
    description text,
    state integer
);


ALTER TABLE public.city OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 29595)
-- Name: city_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.city_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.city_id_seq OWNER TO postgres;

--
-- TOC entry 2965 (class 0 OID 0)
-- Dependencies: 203
-- Name: city_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.city_id_seq OWNED BY public.city.id;


--
-- TOC entry 208 (class 1259 OID 29615)
-- Name: country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.country (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    name text,
    description text
);


ALTER TABLE public.country OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 29602)
-- Name: country_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.country_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.country_id_seq OWNER TO postgres;

--
-- TOC entry 2966 (class 0 OID 0)
-- Dependencies: 205
-- Name: country_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.country_id_seq OWNED BY public.country.id;


--
-- TOC entry 211 (class 1259 OID 29634)
-- Name: domain; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.domain (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    name text,
    description text
);


ALTER TABLE public.domain OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 29619)
-- Name: domain_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.domain_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.domain_id_seq OWNER TO postgres;

--
-- TOC entry 2967 (class 0 OID 0)
-- Dependencies: 209
-- Name: domain_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.domain_id_seq OWNED BY public.domain.id;


--
-- TOC entry 210 (class 1259 OID 29629)
-- Name: domainvalue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.domainvalue (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    key text,
    value text,
    description text,
    domain integer
);


ALTER TABLE public.domainvalue OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 29614)
-- Name: domainvalue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.domainvalue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.domainvalue_id_seq OWNER TO postgres;

--
-- TOC entry 2968 (class 0 OID 0)
-- Dependencies: 207
-- Name: domainvalue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.domainvalue_id_seq OWNED BY public.domainvalue.id;


--
-- TOC entry 214 (class 1259 OID 29649)
-- Name: form; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.form (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    name text,
    description text,
    "isPublic" boolean,
    "isActive" boolean,
    "isPublished" boolean,
    header text,
    footer text
);


ALTER TABLE public.form OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 29641)
-- Name: form_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.form_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.form_id_seq OWNER TO postgres;

--
-- TOC entry 2969 (class 0 OID 0)
-- Dependencies: 212
-- Name: form_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.form_id_seq OWNED BY public.form.id;


--
-- TOC entry 216 (class 1259 OID 29654)
-- Name: record; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.record (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    form integer
);


ALTER TABLE public.record OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 29647)
-- Name: record_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.record_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.record_id_seq OWNER TO postgres;

--
-- TOC entry 2970 (class 0 OID 0)
-- Dependencies: 213
-- Name: record_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.record_id_seq OWNED BY public.record.id;


--
-- TOC entry 219 (class 1259 OID 29685)
-- Name: selectoption; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.selectoption (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    key text,
    value text,
    input integer
);


ALTER TABLE public.selectoption OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 29683)
-- Name: selectoption_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.selectoption_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.selectoption_id_seq OWNER TO postgres;

--
-- TOC entry 2971 (class 0 OID 0)
-- Dependencies: 218
-- Name: selectoption_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.selectoption_id_seq OWNED BY public.selectoption.id;


--
-- TOC entry 221 (class 1259 OID 29700)
-- Name: state; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.state (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    name text,
    description text,
    country integer
);


ALTER TABLE public.state OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 29698)
-- Name: state_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.state_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.state_id_seq OWNER TO postgres;

--
-- TOC entry 2972 (class 0 OID 0)
-- Dependencies: 220
-- Name: state_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.state_id_seq OWNED BY public.state.id;


--
-- TOC entry 217 (class 1259 OID 29663)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    "createdAt" bigint,
    "updatedAt" bigint,
    id integer NOT NULL,
    "emailAddress" text,
    photo json,
    "isActive" boolean,
    color text,
    "canLogIn" boolean,
    password text,
    "fullName" text,
    "isSuperAdmin" boolean,
    "passwordResetToken" text,
    "passwordResetTokenExpiresAt" real,
    "stripeCustomerId" text,
    "hasBillingCard" boolean,
    "billingCardBrand" text,
    "billingCardLast4" text,
    "billingCardExpMonth" text,
    "billingCardExpYear" text,
    "emailProofToken" text,
    "emailProofTokenExpiresAt" real,
    "emailStatus" text,
    "emailChangeCandidate" text,
    "tosAcceptedByIp" text,
    "lastSeenAt" real,
    "signupTime" real,
    "signedupBy" integer
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 29652)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- TOC entry 2973 (class 0 OID 0)
-- Dependencies: 215
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- TOC entry 2762 (class 2604 OID 29590)
-- Name: answer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer ALTER COLUMN id SET DEFAULT nextval('public.answer_id_seq'::regclass);


--
-- TOC entry 2774 (class 2604 OID 29716)
-- Name: archive id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive ALTER COLUMN id SET DEFAULT nextval('public.archive_id_seq'::regclass);


--
-- TOC entry 2764 (class 2604 OID 29608)
-- Name: bodysection id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bodysection ALTER COLUMN id SET DEFAULT nextval('public.bodysection_id_seq'::regclass);


--
-- TOC entry 2763 (class 2604 OID 29601)
-- Name: bodysectioninput id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bodysectioninput ALTER COLUMN id SET DEFAULT nextval('public.bodysectioninput_id_seq'::regclass);


--
-- TOC entry 2761 (class 2604 OID 19256)
-- Name: category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);


--
-- TOC entry 2765 (class 2604 OID 29618)
-- Name: city id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.city ALTER COLUMN id SET DEFAULT nextval('public.city_id_seq'::regclass);


--
-- TOC entry 2766 (class 2604 OID 29626)
-- Name: country id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.country ALTER COLUMN id SET DEFAULT nextval('public.country_id_seq'::regclass);


--
-- TOC entry 2768 (class 2604 OID 29639)
-- Name: domain id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domain ALTER COLUMN id SET DEFAULT nextval('public.domain_id_seq'::regclass);


--
-- TOC entry 2767 (class 2604 OID 29637)
-- Name: domainvalue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domainvalue ALTER COLUMN id SET DEFAULT nextval('public.domainvalue_id_seq'::regclass);


--
-- TOC entry 2769 (class 2604 OID 29653)
-- Name: form id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.form ALTER COLUMN id SET DEFAULT nextval('public.form_id_seq'::regclass);


--
-- TOC entry 2770 (class 2604 OID 29660)
-- Name: record id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.record ALTER COLUMN id SET DEFAULT nextval('public.record_id_seq'::regclass);


--
-- TOC entry 2772 (class 2604 OID 29688)
-- Name: selectoption id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.selectoption ALTER COLUMN id SET DEFAULT nextval('public.selectoption_id_seq'::regclass);


--
-- TOC entry 2773 (class 2604 OID 29703)
-- Name: state id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.state ALTER COLUMN id SET DEFAULT nextval('public.state_id_seq'::regclass);


--
-- TOC entry 2771 (class 2604 OID 29667)
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 2930 (class 0 OID 29585)
-- Dependencies: 200
-- Data for Name: answer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.answer ("createdAt", "updatedAt", id, value, input, record) FROM stdin;
1534361170227	1534361170227	1	Cesar	1	1
1534361186981	1534361186981	3	a@a.a	3	1
1534361200254	1534361200254	4	18	4	1
1534361206413	1534361206413	5	1000	5	1
1534202462336	1534202462336	13	Masculino	14	1
1534202462336	1534202462336	14	Lina	1	2
1534202462336	1534202462336	16	b@b.b	3	2
1534202462336	1534202462336	17	24	4	2
1534202462336	1534202462336	18	2000	5	2
1534202462336	1534202462336	26	Femenino	14	2
1534202462336	1534202462336	27	Alejandro	1	3
1534202462336	1534202462336	29	c@c.c	3	3
1534202462336	1534202462336	30	30	4	3
1534202462336	1534202462336	31	3000	5	3
1534202462336	1534202462336	39	Masculino	14	3
1534202462336	1534202462336	40	Marcela	1	4
1534202462336	1534202462336	42	d@d.d	3	4
1534202462336	1534202462336	43	16	4	4
1534202462336	1534202462336	44	4000	5	4
1534202462336	1534202462336	52	Femenino	14	4
1534202462336	1534202462336	53	Daniel	1	5
1534202462336	1534202462336	54	f@f.f	3	5
1534202462336	1534202462336	55	29	4	5
1534202462336	1534202462336	56	5000	5	5
1534202462336	1534202462336	57	Masculino	14	5
1534202462336	1534202462336	58	Monica	1	6
1534202462336	1534202462336	59	g@g.g	3	6
1534202462336	1534202462336	60	20	4	6
1534202462336	1534202462336	61	6000	5	6
1534202462336	1534202462336	62	Femenino	14	6
\.


--
-- TOC entry 2953 (class 0 OID 29713)
-- Dependencies: 223
-- Data for Name: archive; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.archive (id, "createdAt", "fromModel", "originalRecord", "originalRecordId") FROM stdin;
\.


--
-- TOC entry 2934 (class 0 OID 29600)
-- Dependencies: 204
-- Data for Name: bodysection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bodysection ("createdAt", "updatedAt", id, name, description, "order", form, "bodySection") FROM stdin;
1533739664576	1533739664576	1			1	1	\N
\.


--
-- TOC entry 2932 (class 0 OID 29592)
-- Dependencies: 202
-- Data for Name: bodysectioninput; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.bodysectioninput ("createdAt", "updatedAt", id, "inputType", label, "order", help, "isRequired", "includeInFilters", "maxLength", "minLength", "maxValue", "minValue", is12h, "selectType", "canTypeOther", "labelCanTypeOther", "yesText", "noText", "minStep", "maxStep", "stepValue", "editableLocation", "photoSource", "imageQuality", domain, "bodySection") FROM stdin;
1534200046600	1534200046600	1	text	Nombre	1		f	f	\N	\N	\N	\N	f	radio	f		Si	No	0	10	1	f	both	40	\N	1
1534200118646	1534200118646	3	e-mail	E-mail	3		f	f	\N	\N	\N	\N	f	radio	f		Si	No	0	10	1	f	both	40	\N	1
1534200127092	1534200127092	4	number	Edad	4		f	f	\N	\N	\N	\N	f	radio	f		Si	No	0	10	1	f	both	40	\N	1
1534200139800	1534200139800	5	money	Saldo	5		f	f	\N	\N	\N	\N	f	radio	f		Si	No	0	10	1	f	both	40	\N	1
1534204871642	1534204871642	14	gender	Genero	13		f	f	\N	\N	\N	\N	f	radio	f		Si	No	0	10	1	f	both	40	\N	1
\.


--
-- TOC entry 2927 (class 0 OID 19251)
-- Dependencies: 197
-- Data for Name: category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.category ("createdAt", "updatedAt", id, name, description) FROM stdin;
\.


--
-- TOC entry 2936 (class 0 OID 29609)
-- Dependencies: 206
-- Data for Name: city; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.city ("createdAt", "updatedAt", id, name, description, state) FROM stdin;
\.


--
-- TOC entry 2938 (class 0 OID 29615)
-- Dependencies: 208
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.country ("createdAt", "updatedAt", id, name, description) FROM stdin;
\.


--
-- TOC entry 2941 (class 0 OID 29634)
-- Dependencies: 211
-- Data for Name: domain; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.domain ("createdAt", "updatedAt", id, name, description) FROM stdin;
\.


--
-- TOC entry 2940 (class 0 OID 29629)
-- Dependencies: 210
-- Data for Name: domainvalue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.domainvalue ("createdAt", "updatedAt", id, key, value, description, domain) FROM stdin;
\.


--
-- TOC entry 2944 (class 0 OID 29649)
-- Dependencies: 214
-- Data for Name: form; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.form ("createdAt", "updatedAt", id, name, description, "isPublic", "isActive", "isPublished", header, footer) FROM stdin;
1532787539427	1534435840668	1	Formulario 1	123	t	f	f	<p>akjsnda sd a sdkj akjs da</p><p><br data-mce-bogus="1"></p><p style="padding-left: 30px;" data-mce-style="padding-left: 30px;">asdasdasd</p>	
\.


--
-- TOC entry 2946 (class 0 OID 29654)
-- Dependencies: 216
-- Data for Name: record; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.record ("createdAt", "updatedAt", id, form) FROM stdin;
1534361129370	1534361129370	1	1
1534361141865	1534361141865	2	1
1534361148241	1534361148241	3	1
1534361160578	1534361160578	4	1
1534361160578	1534361160578	5	1
1534361160578	1534361160578	6	1
\.


--
-- TOC entry 2949 (class 0 OID 29685)
-- Dependencies: 219
-- Data for Name: selectoption; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.selectoption ("createdAt", "updatedAt", id, key, value, input) FROM stdin;
1534203746364	1534203746364	1	uno	uno	11
1534203746364	1534203746364	2	dos	dos	11
1534203746364	1534203746364	3	tres	tres	11
1534204400440	1534204400440	4	cuatro	cuatro	12
1534204400440	1534204400440	5	cinco	cinco	12
1534204400440	1534204400440	6	seis	seis	12
\.


--
-- TOC entry 2951 (class 0 OID 29700)
-- Dependencies: 221
-- Data for Name: state; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.state ("createdAt", "updatedAt", id, name, description, country) FROM stdin;
\.


--
-- TOC entry 2947 (class 0 OID 29663)
-- Dependencies: 217
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" ("createdAt", "updatedAt", id, "emailAddress", photo, "isActive", color, "canLogIn", password, "fullName", "isSuperAdmin", "passwordResetToken", "passwordResetTokenExpiresAt", "stripeCustomerId", "hasBillingCard", "billingCardBrand", "billingCardLast4", "billingCardExpMonth", "billingCardExpYear", "emailProofToken", "emailProofTokenExpiresAt", "emailStatus", "emailChangeCandidate", "tosAcceptedByIp", "lastSeenAt", "signupTime", "signedupBy") FROM stdin;
1532787482092	1532787482092	1	admin@example.com	\N	t		t	$2a$10$cWdkMM4s7cJQ9glAMR.64ubAoRpdkieQpF1CNeBFNJluQg56vL/T6	Ryan Dahl	t		0		f						0	confirmed			0	0	\N
1532787520923	1534440486804	2	cdavidcvalencia@gmail.com	\N	t		t	$2a$10$kBoGsZurNBlk4n3VsLNey.cxAkU7bAcNX/FIIdJB1ej8sFznNSg6u	César David Castro Valencia	f		0		f						0	confirmed		::1	1.53444051e+012	0	\N
\.


--
-- TOC entry 2974 (class 0 OID 0)
-- Dependencies: 198
-- Name: answer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.answer_id_seq', 62, true);


--
-- TOC entry 2975 (class 0 OID 0)
-- Dependencies: 222
-- Name: archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.archive_id_seq', 1, false);


--
-- TOC entry 2976 (class 0 OID 0)
-- Dependencies: 201
-- Name: bodysection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bodysection_id_seq', 1, true);


--
-- TOC entry 2977 (class 0 OID 0)
-- Dependencies: 199
-- Name: bodysectioninput_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.bodysectioninput_id_seq', 14, true);


--
-- TOC entry 2978 (class 0 OID 0)
-- Dependencies: 196
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.category_id_seq', 1, false);


--
-- TOC entry 2979 (class 0 OID 0)
-- Dependencies: 203
-- Name: city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.city_id_seq', 1, false);


--
-- TOC entry 2980 (class 0 OID 0)
-- Dependencies: 205
-- Name: country_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.country_id_seq', 1, false);


--
-- TOC entry 2981 (class 0 OID 0)
-- Dependencies: 209
-- Name: domain_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.domain_id_seq', 1, false);


--
-- TOC entry 2982 (class 0 OID 0)
-- Dependencies: 207
-- Name: domainvalue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.domainvalue_id_seq', 1, false);


--
-- TOC entry 2983 (class 0 OID 0)
-- Dependencies: 212
-- Name: form_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.form_id_seq', 11, true);


--
-- TOC entry 2984 (class 0 OID 0)
-- Dependencies: 213
-- Name: record_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.record_id_seq', 6, true);


--
-- TOC entry 2985 (class 0 OID 0)
-- Dependencies: 218
-- Name: selectoption_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.selectoption_id_seq', 6, true);


--
-- TOC entry 2986 (class 0 OID 0)
-- Dependencies: 220
-- Name: state_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.state_id_seq', 1, false);


--
-- TOC entry 2987 (class 0 OID 0)
-- Dependencies: 215
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 2, true);


--
-- TOC entry 2778 (class 2606 OID 29676)
-- Name: answer answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_pkey PRIMARY KEY (id);


--
-- TOC entry 2804 (class 2606 OID 29723)
-- Name: archive archive_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.archive
    ADD CONSTRAINT archive_pkey PRIMARY KEY (id);


--
-- TOC entry 2782 (class 2606 OID 29677)
-- Name: bodysection bodysection_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bodysection
    ADD CONSTRAINT bodysection_pkey PRIMARY KEY (id);


--
-- TOC entry 2780 (class 2606 OID 29680)
-- Name: bodysectioninput bodysectioninput_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.bodysectioninput
    ADD CONSTRAINT bodysectioninput_pkey PRIMARY KEY (id);


--
-- TOC entry 2776 (class 2606 OID 19330)
-- Name: category category_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);


--
-- TOC entry 2784 (class 2606 OID 29679)
-- Name: city city_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (id);


--
-- TOC entry 2786 (class 2606 OID 29678)
-- Name: country country_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (id);


--
-- TOC entry 2790 (class 2606 OID 29697)
-- Name: domain domain_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domain
    ADD CONSTRAINT domain_pkey PRIMARY KEY (id);


--
-- TOC entry 2788 (class 2606 OID 29696)
-- Name: domainvalue domainvalue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.domainvalue
    ADD CONSTRAINT domainvalue_pkey PRIMARY KEY (id);


--
-- TOC entry 2792 (class 2606 OID 29695)
-- Name: form form_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.form
    ADD CONSTRAINT form_pkey PRIMARY KEY (id);


--
-- TOC entry 2794 (class 2606 OID 29666)
-- Name: record record_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.record
    ADD CONSTRAINT record_pkey PRIMARY KEY (id);


--
-- TOC entry 2800 (class 2606 OID 29712)
-- Name: selectoption selectoption_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.selectoption
    ADD CONSTRAINT selectoption_pkey PRIMARY KEY (id);


--
-- TOC entry 2802 (class 2606 OID 29721)
-- Name: state state_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.state
    ADD CONSTRAINT state_pkey PRIMARY KEY (id);


--
-- TOC entry 2796 (class 2606 OID 29709)
-- Name: user user_emailAddress_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "user_emailAddress_key" UNIQUE ("emailAddress");


--
-- TOC entry 2798 (class 2606 OID 29682)
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


-- Completed on 2018-08-16 12:35:07

--
-- PostgreSQL database dump complete
--

